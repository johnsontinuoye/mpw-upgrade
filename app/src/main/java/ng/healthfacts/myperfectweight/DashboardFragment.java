package ng.healthfacts.myperfectweight;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AlertDialog.Builder;

import java.time.temporal.ChronoUnit;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.concurrent.TimeUnit;

public class DashboardFragment
  extends Fragment
{
  public static final String Selection = "weightKey";
  public static final String mypreference = "mypref";
  SharedPreferences sharedpreferences;
  TextView t1;
  TextView t2;
  String weightKey;
  Intent intent;
  Integer cDay, cMonth, cYear;
  Integer day;
  
  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    return paramLayoutInflater.inflate(R.layout.fragment_dashboard, paramViewGroup, false);
  }
  
  public void onStart()
  {
    super.onStart();
    ((Toolbar)getActivity().findViewById(R.id.toolbar)).setTitle(R.string.title_dashboard);

    DashboardFragment.this.sharedpreferences = DashboardFragment.this.getActivity().getSharedPreferences("mypref", 0);
    weightKey = DashboardFragment.this.sharedpreferences.getString("weightKey", "");
    String str = "<html><head><style type=\"text/css\">@font-face {font-family: MyFont;src: url(\"file:///android_asset/fonts/GothamLight.ttf\")}body {font-family: MyFont;font-size: medium;text-align: center; color: #FFF; background-color: #00a852; line-height: 1.5em; font-weight:500;}</style></head><body>" + "Hi there!, If you 're reading this, it's probably because you want to lose weight or gain weight.Well, Congratulations! You're in the right place. To effectively lose weight or gain weight, here's what you need to know." + "</body></html>";
    ((WebView)getActivity().findViewById(R.id.webView1)).loadDataWithBaseURL(null, str, "text/html", "UTF-8", null);
    str = "<html><head><style type=\"text/css\">@font-face {font-family: MyFont;src: url(\"file:///android_asset/fonts/GothamLight.ttf\")}body {font-family: MyFont;font-size: medium;text-align: center; color: #000; line-height: 1.5em; font-weight:500;}</style></head><body>" + "Weight loss/weight gain is basically about calories (the number of calories you take in daily as well as the number of calories you burn during the cause of the day. To lose weight, you're expected to consume fewer calories than what the body requires for daily maintenance. That way, the body converts the extra fat in your body into calories to be burned for the entire day. Weight gain, on the other hand, requires taking excess calories than your body requires for daily maintenance so that the excess is being stored as fat in the body. MyPerfectWeight app has been designed and programmed to help you achieve your goals (weight loss/weight gain) in a healthy manner with food items readily available to any Nigerian within a period of 30 days (or more, depending on how much weight you want to gain or lose). Be sure to follow the recommended quantity of each meal." + "</body></html>";
    ((WebView)getActivity().findViewById(R.id.webView2)).loadDataWithBaseURL(null, str, "text/html", "UTF-8", null);

    Calendar calandar = Calendar.getInstance();
    cDay = calandar.get(Calendar.DAY_OF_MONTH);
    cMonth = calandar.get(Calendar.MONTH) + 1;
    cYear = calandar.get(Calendar.YEAR);
    Calendar start_day = new GregorianCalendar();
    if(sharedpreferences.getLong("start_day",0L)!=0L){
      start_day.setTimeInMillis(sharedpreferences.getLong("start_day",0));
      long end = calandar.getTimeInMillis();
      long start = start_day.getTimeInMillis();
      Long daysBetween = TimeUnit.MILLISECONDS.toDays(Math.abs(end - start));
      day = daysBetween.intValue() + 1;
    }else{
      weightKey = "";
      day = 1;
    }
    if(day>30){
      weightKey = "";
    }
//    Toast.makeText(getActivity()," " + day,Toast.LENGTH_LONG).show();

    if (weightKey.contains("Gain"))
    {
      String activityToStart = "ng.healthfacts.myperfectweight.gain.Day" + day.toString();
      try {
        Class<?> c = Class.forName(activityToStart);
       intent = new Intent(DashboardFragment.this.getActivity(), c);
        //startActivity(intent);
      } catch (ClassNotFoundException ignored) {
        intent = new Intent(DashboardFragment.this.getActivity(), WeightGainPlan.class);
      }
      ((Button)getView().findViewById(R.id.button1)).setText("Continue");
      str = "<html><head><style type=\"text/css\">@font-face {font-family: MyFont;src: url(\"file:///android_asset/fonts/GothamLight.ttf\")}body {font-family: MyFont;font-size: medium;text-align: center; color: #FFF; background-color: #00a852; line-height: 1.5em; font-weight:500;}</style></head><body>" + "Hi there!, Welcome back. <br/> You are on the weight gain plan. You have made it to day "+ day.toString() + " of the plan.</body></html>";
      ((WebView)getActivity().findViewById(R.id.webView1)).loadDataWithBaseURL(null, str, "text/html", "UTF-8", null);
    }
    else if (weightKey.contains("Fast"))
    {
      String activityToStart = "ng.healthfacts.myperfectweight.fast.FDay" + day.toString();
      try {
        Class<?> c = Class.forName(activityToStart);
        intent = new Intent(DashboardFragment.this.getActivity(), c);
        //startActivity(intent);
      } catch (ClassNotFoundException ignored) {
        intent = new Intent(DashboardFragment.this.getActivity(), WeightLossFastPlan.class);
      }
      ((Button)getView().findViewById(R.id.button1)).setText("Continue");
      str = "<html><head><style type=\"text/css\">@font-face {font-family: MyFont;src: url(\"file:///android_asset/fonts/GothamLight.ttf\")}body {font-family: MyFont;font-size: medium;text-align: center; color: #FFF; background-color: #00a852; line-height: 1.5em; font-weight:500;}</style></head><body>" + "Hi there!, Welcome back. <br/> You are on the fast weight loss plan. You have made it to day "+ day.toString() + " of the plan.</body></html>";
      ((WebView)getActivity().findViewById(R.id.webView1)).loadDataWithBaseURL(null, str, "text/html", "UTF-8", null);
    }
    else if (weightKey.contains("Slow"))
    {
      String activityToStart = "ng.healthfacts.myperfectweight.slow.SDay" + day.toString();
      try {
        Class<?> c = Class.forName(activityToStart);
        intent = new Intent(DashboardFragment.this.getActivity(), c);
        //startActivity(intent);
      } catch (ClassNotFoundException ignored) {
        intent = new Intent(DashboardFragment.this.getActivity(), WeightLossSlowPlan.class);
      }
      ((Button)getView().findViewById(R.id.button1)).setText("Continue");

      str = "<html><head><style type=\"text/css\">@font-face {font-family: MyFont;src: url(\"file:///android_asset/fonts/GothamLight.ttf\")}body {font-family: MyFont;font-size: medium;text-align: center; color: #FFF; background-color: #00a852; line-height: 1.5em; font-weight:500;}</style></head><body>" + "Hi there!, Welcome back. <br/> You are on the slow weight loss plan. You have made it to day "+ day.toString() + " of the plan.</body></html>";
      ((WebView)getActivity().findViewById(R.id.webView1)).loadDataWithBaseURL(null, str, "text/html", "UTF-8", null);
    }
    else{
       intent = new Intent(DashboardFragment.this.getActivity(), PlanSelectionActivity.class);
      ((Button)getView().findViewById(R.id.button2)).setVisibility(View.GONE);
       weightKey = "no";

    }

    ((Button)getView().findViewById(R.id.button1)).setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        Toast.makeText(DashboardFragment.this.getActivity(), "You are on " + weightKey + " Plan", Toast.LENGTH_SHORT).show();

        DashboardFragment.this.startActivity(intent);
      }
    });
    ((Button)getView().findViewById(R.id.button2)).setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        new AlertDialog.Builder(DashboardFragment.this.getActivity()).setTitle("New Plan").setMessage("This will cancel your current plan")
                .setPositiveButton("Continue", new DialogInterface.OnClickListener(){
                  @Override
                  public void onClick(DialogInterface dialogInterface, int i){
                    SharedPreferences.Editor localEditor = sharedpreferences.edit();
                    localEditor.putString("weightKey", "");
                    localEditor.putLong("start_day", 0L);
                    localEditor.commit();
                    intent = new Intent(DashboardFragment.this.getActivity(), PlanSelectionActivity.class);
                    ((Button)getView().findViewById(R.id.button1)).setVisibility(View.GONE);
                    Toast.makeText(DashboardFragment.this.getActivity(), "You have successfully cancelled your current plan.", Toast.LENGTH_SHORT).show();

                    DashboardFragment.this.startActivity(intent);
                  }
                }).setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
          @Override
          public void onClick(DialogInterface dialogInterface, int i) {
            dialogInterface.cancel();
          }
        }).create().show();
                }
    });
  }
}


/* DashboardFragment.class
 */