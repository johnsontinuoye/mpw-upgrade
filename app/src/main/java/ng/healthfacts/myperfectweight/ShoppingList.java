package ng.healthfacts.myperfectweight;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import net.cachapa.expandablelayout.ExpandableLayout;

public class ShoppingList
  extends AppCompatActivity
{
  private Toolbar mToolbar;
  private CardView carbonhydrateCard;
  private ExpandableLayout expandableCarbonhydrateLayout;
  private CardView proteinCard;
  private ExpandableLayout expandableProteinLayout;
  private CardView fruitCard;
  private ExpandableLayout expandableFruitLayout;
  private CardView vegetableCard;
  private ExpandableLayout expandableVegetableLayout;
  private BottomNavigationView navigation;
  private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
          = item -> {
    switch (item.getItemId()) {
      case R.id.navigation_dashboard:
//                        switchFragment( new DashboardFragment() );
        startActivity(new Intent(ShoppingList.this, MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION));
        return true;
      case R.id.navigation_shopping_list:
        //switchFragment( new DailyFoodFragment() );
       // startActivity(new Intent(ShoppingList.this, ShoppingList.class));
        return true;
      case R.id.navigation_calorie:
        startActivity(new Intent(ShoppingList.this, CaloriesActivity.class).addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION));
        return true;
      case R.id.navigation_bmi:
        startActivity(new Intent(ShoppingList.this, BMIActivity.class).addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION));
        return true;
    }
    return false;
  };


  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setContentView(R.layout.activity_shopping_list);
    this.mToolbar = ((Toolbar)findViewById(R.id.toolbar));
    setSupportActionBar(this.mToolbar);
    this.mToolbar.setTitle("Shopping List");
    getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    navigation = (BottomNavigationView) findViewById(R.id.navigation);
    navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
    navigation.setSelectedItemId(R.id.navigation_shopping_list);
    carbonhydrateCard= (CardView)findViewById(R.id.carbonhydrate_card);
    expandableCarbonhydrateLayout = (ExpandableLayout)findViewById(R.id.expandable_carbohydrate_layout);


    ImageView imageAnim =  (ImageView) findViewById(R.id.imageView3);
    new CountDownTimer(1000, 500) {

      public void onTick(long millisUntilFinished) {
      }

      public void onFinish() {
        Animation in  = AnimationUtils.loadAnimation(ShoppingList.this, R.anim.anim_calories_head);
        imageAnim.setAnimation(in);
        imageAnim.setImageResource(R.drawable.ic_local_grocery_store_head);
      }
    }.start();

    carbonhydrateCard.setOnClickListener(v -> {
      if (expandableCarbonhydrateLayout.isExpanded()) {
        expandableCarbonhydrateLayout.collapse();
//        imageView.setImageResource(R.drawable.ic_add_black_24dp);
//        imageView.setAnimation();
      } else {
        expandableCarbonhydrateLayout.expand();
//        imageView.setImageResource(R.drawable.ic_remove_24dp);
//        imageView.setAnimation(in);
      }
    });
    proteinCard= (CardView)findViewById(R.id.protein_card);
    expandableProteinLayout = (ExpandableLayout)findViewById(R.id.expandable_protein_layout);

    proteinCard.setOnClickListener(v -> {
      if (expandableProteinLayout.isExpanded()) {
        expandableProteinLayout.collapse();
//        imageView.setImageResource(R.drawable.ic_add_black_24dp);
//        imageView.setAnimation();
      } else {
        expandableProteinLayout.expand();
//        imageView.setImageResource(R.drawable.ic_remove_24dp);
//        imageView.setAnimation(in);
      }
    });
    fruitCard= (CardView)findViewById(R.id.fruit_card);
    expandableFruitLayout = (ExpandableLayout)findViewById(R.id.expandable_fruit_layout);

    fruitCard.setOnClickListener(v -> {
      if (expandableFruitLayout.isExpanded()) {
        expandableFruitLayout.collapse();
//        imageView.setImageResource(R.drawable.ic_add_black_24dp);
//        imageView.setAnimation();
      } else {
        expandableFruitLayout.expand();
//        imageView.setImageResource(R.drawable.ic_remove_24dp);
//        imageView.setAnimation(in);
      }
    });
    vegetableCard= (CardView)findViewById(R.id.vegetable_card);
    expandableVegetableLayout = (ExpandableLayout)findViewById(R.id.expandable_vegetable_layout);

    vegetableCard.setOnClickListener(v -> {
      if (expandableVegetableLayout.isExpanded()) {
        expandableVegetableLayout.collapse();
//        imageView.setImageResource(R.drawable.ic_add_black_24dp);
//        imageView.setAnimation();
      } else {
        expandableVegetableLayout.expand();
//        imageView.setImageResource(R.drawable.ic_remove_24dp);
//        imageView.setAnimation(in);
      }
    });
  }

  @Override
  protected void onResume() {
    super.onResume();
    navigation.setSelectedItemId(R.id.navigation_shopping_list);
  }
}
