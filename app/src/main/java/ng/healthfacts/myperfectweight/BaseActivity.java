package ng.healthfacts.myperfectweight;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

public abstract class BaseActivity
  extends AppCompatActivity
{
  Toolbar toolbar;
  
  private void configureToolbar()
  {
    this.toolbar = ((Toolbar)findViewById(R.id.toolbar));
    if (this.toolbar != null)
    {
      setSupportActionBar(this.toolbar);
      getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }
  }
  
  protected abstract int getLayoutResource();
  
  public void onCreate(@Nullable Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setContentView(getLayoutResource());
    configureToolbar();
  }
  
  public boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    switch (paramMenuItem.getItemId())
    {
    default: 
      return super.onOptionsItemSelected(paramMenuItem);
    }
//    paramMenuItem = getSupportFragmentManager();
//    if ((paramMenuItem != null) && (paramMenuItem.getBackStackEntryCount() > 0))
//    {
//      paramMenuItem.popBackStack(null, 1);
//      return true;
//    }
//    finish();
//    return true;
  }
}


/* BaseActivity.class
 */