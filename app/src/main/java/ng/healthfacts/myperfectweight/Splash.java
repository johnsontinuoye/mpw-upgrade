package ng.healthfacts.myperfectweight;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

public class Splash
  extends AppCompatActivity
{
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setContentView(R.layout.activity_splash);
    new Thread()
    {
      public void run()
      {
        try
        {
          sleep(2000L);
          Splash.this.onShouldStartActivity();
          return;
        }
        catch (InterruptedException localInterruptedException)
        {
          return;
        }
        finally
        {
          Splash.this.finish();
        }
      }
    }.start();
  }
  
  public void onShouldStartActivity()
  {
    Intent intent = new Intent(this, MainActivity.class);
    intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
    startActivity(intent);
  }
}
