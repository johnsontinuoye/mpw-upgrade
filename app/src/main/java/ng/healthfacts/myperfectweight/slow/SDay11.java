package ng.healthfacts.myperfectweight.slow;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.Toolbar;

import ng.healthfacts.myperfectweight.R;

public class SDay11
  extends AppCompatActivity
{
  private GridLayoutManager lLayout;
  private Toolbar mToolbar;
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setContentView(R.layout.sactivity_day11);
    this.mToolbar = ((Toolbar)findViewById(R.id.toolbar));
    setSupportActionBar(this.mToolbar);
    getSupportActionBar().setDisplayHomeAsUpEnabled(true);
  }
}


/* slow\SDay11.class
 */