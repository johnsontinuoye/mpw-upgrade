package ng.healthfacts.myperfectweight.helper;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.google.gson.Gson;

import java.util.Date;

import ng.healthfacts.myperfectweight.model.User;

/**
 * Created by Afolayan Oluwaseyi on 1/18/18.
 */

public class AccountUtils {
    public static final String USER_EMAIL = "user_email";
    public static final String USER_PASSWORD = "user_password";
    public static final String USER_NAME = "user_name";
    public static final String USER_ID = "user_id";
    public static final String USER_LOGIN = "user_login";
    public static final String USER_PLAN = "user_plan";
    public static final String USER_PLAN_START_DATE = "user_plan_start_date";
    public static final String USER_PLAN_CURRENT_DATE = "user_plan_current_date";


    public static void setUser(Context context, String userId, String email, String password, String userName){
        setUserId(context, userId);
        setUserEmail(context, email);
        setUserPassword(context, password);
        setUserName(context, userName);
        setLoggedIn(context, true);
    }
    public static void setUserEmail(Context context, String email){
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        preferences.edit().putString(USER_EMAIL,email).apply();
    }

    public static String getUserEmail(Context context){
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getString(USER_EMAIL, "");
    }

    public static void setLoggedIn(Context context, boolean loggedIn){
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        preferences.edit().putBoolean(USER_LOGIN, loggedIn).apply();
    }

    public static boolean hasLoggedIn(Context context){
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getBoolean(USER_LOGIN, false);
    }

    public static void setUserPassword(Context context, String password){
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        preferences.edit().putString(USER_PASSWORD, password).apply();
    }

    public static String getUserPassword(Context context){
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getString(USER_PASSWORD, "");
    }

    public static void setUserName(Context context, String userName){
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        preferences.edit().putString(USER_NAME,userName).apply();
    }

    public static String getUserName(Context context){
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getString(USER_NAME, "");
    }

    public static void setUserPlan(Context context, String userPlan){
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        preferences.edit().putString(USER_PLAN, userPlan).apply();
    }

    public static String getUserPlan(Context context){
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getString(USER_PLAN, "");
    }

    public static void setUserPlanCurrentDay(Context context, String userCurrentDay){
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        preferences.edit().putString(USER_PLAN_CURRENT_DATE, userCurrentDay).apply();
    }

    public static String getUserPlanCurrentDay(Context context){
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getString(USER_PLAN_CURRENT_DATE, "");
    }

    public static void setUserPlanStartDate(Context context, Date userPlanStart){
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        if( userPlanStart != null )
            preferences.edit().putLong(USER_PLAN_START_DATE, userPlanStart.getTime()).apply();
    }

    public static Date getUserPlanStartDate(Context context){
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        long dateLong = preferences.getLong(USER_PLAN_START_DATE, System.currentTimeMillis());
        return new Date(dateLong);
    }

    public static void setUserId(Context context, String userId){
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        preferences.edit().putString(USER_ID, userId).apply();
    }

    public static String getUserId(Context context){
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getString(USER_ID, "");
    }
    public static void setUser(Context context, User user){
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor prefsEditor = preferences.edit();
        Gson gson = new Gson();
        String json = gson.toJson(user);
        prefsEditor.putString("SerializableObject", json);
        prefsEditor.apply();
    }

    public static User getUser(Context context){
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        Gson gson = new Gson();
        String json = preferences.getString("SerializableObject", "");
        return gson.fromJson(json, User.class);
    }

    public static void deleteData(Context context){
        setUserId(context, null);
        setUserEmail(context, null);
        setUserPassword(context, null);
        setUserName(context, null);
        setLoggedIn(context, false);
    }
}
