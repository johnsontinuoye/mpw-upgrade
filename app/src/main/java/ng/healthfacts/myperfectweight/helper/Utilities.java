package ng.healthfacts.myperfectweight.helper;

import android.text.TextUtils;

import java.text.DateFormat;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Afolayan Oluwaseyi on 1/18/18.
 */

public class Utilities {

    /**(447.593 + (9.247 * weight (kg)) + (3.098 * height (cm)) - (4.33 * age)) * Activity level = TEE
     *TEE - Total Energy Expenditure which is the same as Daily Maintenance Calorie
     *
     * Sedentary (Little to none) - 1.2
     Light (1-3 days) - 1.375
     Moderate (3-5 days) - 1.55
     Heavy (6-7 days) - 1.725
     Very heavy (twice daily) - 1.9
     */
    public static final double TEE_MULTIPLIER = 447.593;
    public static final double WEIGHT_MULTIPLIER = 9.247;
    public static final double HEIGHT_MULTIPLIER = 3.098;
    public static final double AGE_MULTIPLIER = 4.33;

    public static final double SEDENTARY = 1.2;
    public static final double LIGHT = 1.375;
    public static final double MODERATE = 1.55;
    public static final double HEAVY = 1.725;
    public static final double VERY_HEAVY = 1.9;

    public static final String SEDENTARY_TEE = "sedentary";
    public static final String LIGHT_TEE = "light";
    public static final String MODERATE_TEE = "moderate";
    public static final String HEAVY_TEE = "heavy";
    public static final String VERY_HEAVY_TEE = "very_heavy";

    public static final String ENERGY_UNIT_VALUE = "Cal";
    public static final String LOSS_FAST_VALUE = "";
    public static final String LOSS_SLOW_VALUE = "";
    public static final String GAIN_VALUE = "";

    private static final Pattern VALID_EMAIL_ADDRESS_REGEX =
            Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);

    private static boolean validate(String emailStr) {
        Matcher matcher = VALID_EMAIL_ADDRESS_REGEX .matcher(emailStr);
        return matcher.find();
    }
    public static boolean isGenuineEmailAddress(String email) {
        return !TextUtils.isEmpty(email) && validate(email);
        //return !TextUtils.isEmpty(email) && email.contains("@");
    }

    /**
     * @param weight of user in kg
     * @param height of user in cm
     * @param age as int
     * @param activityLevel as string
     * @return the TEE - Total Energy Expenditure
     */
    public static double calculateTEE(double weight, double height, int age, String activityLevel){

        double activityLevelValue;
        if( activityLevel.toLowerCase().contains(SEDENTARY_TEE))
            activityLevelValue = SEDENTARY;
        else if( activityLevel.toLowerCase().contains(LIGHT_TEE))
            activityLevelValue = LIGHT;
        else if( activityLevel.toLowerCase().contains(MODERATE_TEE))
            activityLevelValue = MODERATE;
        else if( activityLevel.toLowerCase().contains(HEAVY_TEE))
            activityLevelValue = HEAVY;
        else if( activityLevel.toLowerCase().contains(VERY_HEAVY_TEE))
            activityLevelValue = VERY_HEAVY;
        else {
            activityLevelValue = SEDENTARY;
        }

        return 447.593 + (WEIGHT_MULTIPLIER * weight) + ( HEIGHT_MULTIPLIER * height )
                - ( AGE_MULTIPLIER * age) * activityLevelValue;

    }

    public static int diffInDays(Date startDate){
        Date presentDate = new Date();
        long startTime = startDate.getTime();
        long endTime = presentDate.getTime();

        long diffTime = endTime - startTime;
        long diffDays = diffTime / (1000 * 60 * 60 * 24);

        DateFormat dateFormat = DateFormat.getDateInstance();
        /*Log.e("Utilities", "diffInDays: between "+ dateFormat.format(startDate)
                +" and "+ dateFormat.format(presentDate)+" is "+diffDays );*/
        return (int)diffDays;
    }

    public static final String WELCOME_NOTE = "Welcome to MyPerfectWeight\n" +
            "Hi there!\n" +
            "If you're reading this, it's probably because you want to lose weight or gain weight.\n" +
            "Well, Congratulations! You're in the right place.\n" +
            "To effectively lose weight or gain weight, here's what you need to know.\n";
    public static final String WELCOME_NOTE_CONT =
            "Weight loss/weight gain is basically about calories (the number of calories you take in daily as\n" +
            "well as the number of calories you burn during the cause of the day.\n" +
            "To lose weight, you're expected to consume fewer calories than what the body requires for daily\n" +
            "maintenance. That way, the body converts the extra fat in your body into calories to be burned\n" +
            "for the entire day.\n" +
            "Weight gain, on the other hand, requires taking excess calories than your body requires for daily\n" +
            "maintenance so that the excess is being stored as fat in the body.\n" +
            "MyPerfectWeight app has been designed and programmed to help you achieve your goals\n" +
            "(weight loss/weight gain) in a healthy manner with food items readily available to any Nigerian\n" +
            "within a period of 30 days (or more, depending on how much weight you want to gain or lose).\n" +
            "Be sure to follow the recommended quantity of each meal.";

    public static final String WEIGHT_LOSS_FOOD = "1. Beans\n" +
            "2. Chicken\n" +
            "3. Rice\n" +
            "4. Yogurt (plain)\n" +
            "5. Wheat flour\n" +
            "6. Wheat pasta\n" +
            "7. Wheat bread\n" +
            "8. Sweet potato\n" +
            "9. Yam\n" +
            "10. Fish\n" +
            "11. Beef\n" +
            "12. Goat meat\n" +
            "13. Eggs\n" +
            "14. Sardine\n" +
            "15. Popcorn\n" +
            "16. Plantain (ripe and unripe)\n" +
            "17. Oatmeal\n" +
            "18. Moi moi\n" +
            "19. Akara\n" +
            "20. Pepper soup\n" +
            "21. Mayonnaise\n" +
            "22. Milk\n" +
            "23. Plantain chips\n";
    public static final String WEIGHT_LOSS_FRUIT = "1. Apples\n" +
            "2. Banana\n" +
            "3. Watermelon\n" +
            "4. Pineapple\n";

    public static final String WEIGHT_LOSS_VEGETABLE = "\n" +
            "1. Carrots\n" +
            "2. Cabbage\n" +
            "3. Vegetables (efo)\n" +
            "4. Okro\n";

    public static final String WEIGHT_GAIN_FOODS = "1. Sweet Potatoes\n" +
            "2. Beans\n" +
            "3. Plantain\n" +
            "4. Whole Wheat Bread\n" +
            "5. Fish\n" +
            "6. Yam\n" +
            "7. Egg\n" +
            "8. Chicken Breast\n" +
            "9. Chicken Drumstick\n" +
            "10. Turkey\n" +
            "11. Beef\n" +
            "12. Oatmeal\n" +
            "13. Rice\n" +
            "14. Ofada Rice\n" +
            "15. Tuna\n" +
            "16. Moin Moin\n" +
            "17. Custard\n" +
            "18. Cornmeal \n" +
            "19. Eba\n" +
            "20. Amala\n" +
            "21. Semovita\n" +
            "22. Cereal\n" +
            "23. Spaghetti\n" +
            "24. Macaroni\n" +
            "25. Noodles\n" +
            "26. Sardine Bread\n" +
            "27. Milk\n" +
            "28. Beverage\n";

    public static final String WEIGHT_GAIN_FRUITS = "1. Apple\n" +
            "2. Banana\n" +
            "3. Orange\n" +
            "4. Mango\n" +
            "5. Avocado\n" +
            "6. Grapefruit\n" +
            "7. Watermelon\n" +
            "8. Cucumber\n" +
            "9. Berries\n" +
            "10. Pineapple\n";

    public static final String WEIGHT_GAIN_VEGETABLES = "1. Spinach\n" +
            "2. Pumpkin (Ugwu) leaves\n" +
            "3. Carrot\n" +
            "4. Bitter leaves\n";

    public static final String WEIGHT_GAIN_SNACK = "1. Yoghurt\n" +
            "2. Smoothies\n" +
            "3. Popcorn\n" +
            "4. Peanut butter\n" +
            "5. Nuts\n" +
            "6. Granola\n" +
            "7. Dark Chocolate\n" +
            "8. Pizza\n" ;

}
