package ng.healthfacts.myperfectweight.app;

import android.content.Context;
import android.support.multidex.MultiDexApplication;

import ng.healthfacts.myperfectweight.R;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by Afolayan Oluwaseyi on 1/15/18.
 */

public class HFWeight extends MultiDexApplication{

    public HFWeight() {
        super();
    }

    @Override
    protected void attachBaseContext(Context base) {
        //super.attachBaseContext(base);
        super.attachBaseContext(CalligraphyContextWrapper.wrap(base));
    }

    @Override
    public void onCreate() {
        super.onCreate();

        CalligraphyConfig.initDefault( new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/Roboto-Light.ttf")
                .setFontAttrId(R.attr.fontPath)
                        .build()
                );
    }
}
