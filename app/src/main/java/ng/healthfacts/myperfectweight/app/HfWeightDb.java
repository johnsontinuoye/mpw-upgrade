package ng.healthfacts.myperfectweight.app;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.TypeConverters;
import android.content.Context;

import ng.healthfacts.myperfectweight.db.DateConverter;
import ng.healthfacts.myperfectweight.db.SuggestedFoodDao;
import ng.healthfacts.myperfectweight.model.SuggestedFood;

/**
 * Created by Afolayan Oluwaseyi on 1/25/18.
 */

@Database(entities = {SuggestedFood.class}, version = 1, exportSchema = false)
@TypeConverters({DateConverter.class})
public abstract class HfWeightDb extends RoomDatabase {


    private static HfWeightDb INSTANCE;

    public static HfWeightDb getDatabase(Context context) {
        if(INSTANCE == null) {
            INSTANCE = Room.databaseBuilder(context.getApplicationContext(), HfWeightDb.class, "weight_db").build();
        }
        return INSTANCE;
    }

    public void destroyInstance() {
        INSTANCE = null;
    }

    public abstract SuggestedFoodDao suggestionsDao();

}