package ng.healthfacts.myperfectweight.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import ng.healthfacts.myperfectweight.R;
import ng.healthfacts.myperfectweight.model.FoodClass;
import ng.healthfacts.myperfectweight.ui.DailyFoodActivity.OnListFragmentInteractionListener;


public class FoodClassRecyclerViewAdapter extends RecyclerView.Adapter<FoodClassRecyclerViewAdapter.ViewHolder> {

    private final List<FoodClass> mValues;
    private final OnListFragmentInteractionListener mListener;

    public FoodClassRecyclerViewAdapter(List<FoodClass> items, OnListFragmentInteractionListener listener) {
        mValues = items;
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_daily_food_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        holder.mFoodQuantity.setText(String.format(Locale.getDefault(), "%d plates", mValues.get(position).getQuantity()));
        holder.mFoodName.setText(mValues.get(position).getFoodClassName());
        SimpleDateFormat dateFormat = new SimpleDateFormat("hh:mm a", Locale.getDefault());
        String dateTaken = dateFormat.format( new Date(mValues.get(position).getTimeTaken()));
        holder.mFoodTime.setText(dateTaken);

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onListFragmentInteraction(holder.mItem);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        final View mView;
        final TextView mFoodTime;
        final TextView mFoodQuantity;
        final TextView mFoodName;
        FoodClass mItem;

        ViewHolder(View view) {
            super(view);
            mView = view;
            mFoodQuantity = (TextView) view.findViewById(R.id.tv_food_quantity);
            mFoodName = (TextView) view.findViewById(R.id.tv_food_name);
            mFoodTime = (TextView) view.findViewById(R.id.tv_food_time);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mFoodName.getText() + "'";
        }
    }
}
