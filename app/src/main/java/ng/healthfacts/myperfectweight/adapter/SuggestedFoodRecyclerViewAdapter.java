package ng.healthfacts.myperfectweight.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import ng.healthfacts.myperfectweight.R;
import ng.healthfacts.myperfectweight.model.SuggestedFood;
import ng.healthfacts.myperfectweight.ui.BackendListActivity.OnListFragmentInteractionListener;


public class SuggestedFoodRecyclerViewAdapter extends RecyclerView.Adapter<SuggestedFoodRecyclerViewAdapter.ViewHolder> {

    public static final int CLICK = 1;
    public static final int LONG_CLICK = 2;

    private final List<SuggestedFood> mValues;
    private final OnListFragmentInteractionListener mListener;

    public SuggestedFoodRecyclerViewAdapter(List<SuggestedFood> items, OnListFragmentInteractionListener listener) {
        mValues = items;
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_suggested_food_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        SuggestedFood suggestedFood = mValues.get(position);
        holder.mBreakfast.setText(String.format("Breakfast: %s", suggestedFood.getBreakfast()));
        holder.mLunch.setText(String.format("Lunch: %s", suggestedFood.getLunch()));
        holder.mDinner.setText(String.format("Dinner: %s", suggestedFood.getDinner()));
        holder.mSnack.setText(String.format("Snack: %s", suggestedFood.getSnack()));
        holder.mSnack1.setText(String.format("Snack 2: %s", suggestedFood.getSnack2()));
        holder.mFruit.setText(String.format("Fruit: %s", suggestedFood.getFruit()));
        holder.mDay.setText(String.format("Day: %s", suggestedFood.getDay()));


        holder.mView.setOnClickListener(v -> {
            if (null != mListener) {
                mListener.onListFragmentInteraction(holder.mItem, CLICK);
            }
        });
        holder.mView.setOnLongClickListener( view -> {
            if (null != mListener) {
                mListener.onListFragmentInteraction(holder.mItem, LONG_CLICK);
            }
            return true;
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        final View mView;
        final TextView mBreakfast, mLunch, mDinner, mSnack,
                mSnack1, mFruit, mDay;
        SuggestedFood mItem;

        ViewHolder(View view) {
            super(view);
            mView = view;
            mBreakfast = (TextView) view.findViewById(R.id.tv_breakfast);
            mLunch = (TextView) view.findViewById(R.id.tv_lunch);
            mDinner = (TextView) view.findViewById(R.id.tv_dinner);
            mSnack = (TextView) view.findViewById(R.id.tv_snack);
            mSnack1 = (TextView) view.findViewById(R.id.tv_snack1);
            mFruit = (TextView) view.findViewById(R.id.tv_fruit);
            mDay = (TextView) view.findViewById(R.id.tv_day);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mBreakfast.getText() + "'";
        }
    }
}
