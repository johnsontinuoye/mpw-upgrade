package ng.healthfacts.myperfectweight;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;
import ng.healthfacts.myperfectweight.slow.SDay1;
import ng.healthfacts.myperfectweight.slow.SDay10;
import ng.healthfacts.myperfectweight.slow.SDay11;
import ng.healthfacts.myperfectweight.slow.SDay12;
import ng.healthfacts.myperfectweight.slow.SDay13;
import ng.healthfacts.myperfectweight.slow.SDay14;
import ng.healthfacts.myperfectweight.slow.SDay15;
import ng.healthfacts.myperfectweight.slow.SDay16;
import ng.healthfacts.myperfectweight.slow.SDay17;
import ng.healthfacts.myperfectweight.slow.SDay18;
import ng.healthfacts.myperfectweight.slow.SDay19;
import ng.healthfacts.myperfectweight.slow.SDay2;
import ng.healthfacts.myperfectweight.slow.SDay20;
import ng.healthfacts.myperfectweight.slow.SDay21;
import ng.healthfacts.myperfectweight.slow.SDay22;
import ng.healthfacts.myperfectweight.slow.SDay23;
import ng.healthfacts.myperfectweight.slow.SDay24;
import ng.healthfacts.myperfectweight.slow.SDay25;
import ng.healthfacts.myperfectweight.slow.SDay26;
import ng.healthfacts.myperfectweight.slow.SDay27;
import ng.healthfacts.myperfectweight.slow.SDay28;
import ng.healthfacts.myperfectweight.slow.SDay29;
import ng.healthfacts.myperfectweight.slow.SDay3;
import ng.healthfacts.myperfectweight.slow.SDay30;
import ng.healthfacts.myperfectweight.slow.SDay4;
import ng.healthfacts.myperfectweight.slow.SDay5;
import ng.healthfacts.myperfectweight.slow.SDay6;
import ng.healthfacts.myperfectweight.slow.SDay7;
import ng.healthfacts.myperfectweight.slow.SDay8;
import ng.healthfacts.myperfectweight.slow.SDay9;

public class RecyclerViewHoldersS
  extends RecyclerView.ViewHolder
  implements View.OnClickListener
{
  public static final String Selection = "weightKey";
  public static final String mypreference = "mypref";
  public Context context;
  public TextView daytitle;
  SharedPreferences sharedpreferences;
  
  public RecyclerViewHoldersS(View paramView)
  {
    super(paramView);
    paramView.setOnClickListener(this);
    this.daytitle = ((TextView)paramView.findViewById(R.id.tvTitle));
  }
  
  public void onClick(View paramView)
  {
    switch (getPosition())
    {
    default: 
      this.context = paramView.getContext();
      this.context.startActivity(new Intent(this.context, SDay30.class));
      return;
    case 0: 
      this.context = paramView.getContext();
      this.context.startActivity(new Intent(this.context, SDay1.class));
      return;
    case 1: 
      this.context = paramView.getContext();
      this.context.startActivity(new Intent(this.context, SDay2.class));
      return;
    case 2: 
      this.context = paramView.getContext();
      this.context.startActivity(new Intent(this.context, SDay3.class));
      return;
    case 3: 
      this.context = paramView.getContext();
      this.context.startActivity(new Intent(this.context, SDay4.class));
      return;
    case 4: 
      this.context = paramView.getContext();
      this.context.startActivity(new Intent(this.context, SDay5.class));
      return;
    case 5: 
      this.context = paramView.getContext();
      this.context.startActivity(new Intent(this.context, SDay6.class));
      return;
    case 6: 
      this.context = paramView.getContext();
      this.context.startActivity(new Intent(this.context, SDay7.class));
      return;
    case 7: 
      this.context = paramView.getContext();
      this.context.startActivity(new Intent(this.context, SDay8.class));
      return;
    case 8: 
      this.context = paramView.getContext();
      this.context.startActivity(new Intent(this.context, SDay9.class));
      return;
    case 9: 
      this.context = paramView.getContext();
      this.context.startActivity(new Intent(this.context, SDay10.class));
      return;
    case 10: 
      this.context = paramView.getContext();
      this.context.startActivity(new Intent(this.context, SDay11.class));
      return;
    case 11: 
      this.context = paramView.getContext();
      this.context.startActivity(new Intent(this.context, SDay12.class));
      return;
    case 12: 
      this.context = paramView.getContext();
      this.context.startActivity(new Intent(this.context, SDay13.class));
      return;
    case 13: 
      this.context = paramView.getContext();
      this.context.startActivity(new Intent(this.context, SDay14.class));
      return;
    case 14: 
      this.context = paramView.getContext();
      this.context.startActivity(new Intent(this.context, SDay15.class));
      return;
    case 15: 
      this.context = paramView.getContext();
      this.context.startActivity(new Intent(this.context, SDay16.class));
      return;
    case 16: 
      this.context = paramView.getContext();
      this.context.startActivity(new Intent(this.context, SDay17.class));
      return;
    case 17: 
      this.context = paramView.getContext();
      this.context.startActivity(new Intent(this.context, SDay18.class));
      return;
    case 18: 
      this.context = paramView.getContext();
      this.context.startActivity(new Intent(this.context, SDay19.class));
      return;
    case 19: 
      this.context = paramView.getContext();
      this.context.startActivity(new Intent(this.context, SDay20.class));
      return;
    case 20: 
      this.context = paramView.getContext();
      this.context.startActivity(new Intent(this.context, SDay21.class));
      return;
    case 21: 
      this.context = paramView.getContext();
      this.context.startActivity(new Intent(this.context, SDay22.class));
      return;
    case 22: 
      this.context = paramView.getContext();
      this.context.startActivity(new Intent(this.context, SDay23.class));
      return;
    case 23: 
      this.context = paramView.getContext();
      this.context.startActivity(new Intent(this.context, SDay24.class));
      return;
    case 24: 
      this.context = paramView.getContext();
      this.context.startActivity(new Intent(this.context, SDay25.class));
      return;
    case 25: 
      this.context = paramView.getContext();
      this.context.startActivity(new Intent(this.context, SDay26.class));
      return;
    case 26: 
      this.context = paramView.getContext();
      this.context.startActivity(new Intent(this.context, SDay27.class));
      return;
    case 27: 
      this.context = paramView.getContext();
      this.context.startActivity(new Intent(this.context, SDay28.class));
      return;
      case 28:
        this.context = paramView.getContext();
        this.context.startActivity(new Intent(this.context, SDay29.class));
    }
  }
}
