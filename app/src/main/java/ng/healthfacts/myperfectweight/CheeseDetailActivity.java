package ng.healthfacts.myperfectweight;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.widget.ImageView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;

public class CheeseDetailActivity
  extends AppCompatActivity
{
  public static final String EXTRA_NAME = "cheese_name";
  
  private void loadBackdrop()
  {
    ImageView localImageView = (ImageView)findViewById(R.id.backdrop);
   // Glide.with(this).load(Integer.valueOf(Cheeses.getRandomCheeseDrawable())).centerCrop().into(localImageView);
    Glide.with(this).load(Integer.valueOf(Cheeses.getRandomCheeseDrawable())).into(localImageView);
  }
  
  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setContentView(R.layout.activity_cheese_detail);
    String title = getIntent().getStringExtra("cheese_name");
    setSupportActionBar((Toolbar)findViewById(R.id.toolbar));
    getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    ((CollapsingToolbarLayout)findViewById(R.id.collapsing_toolbar)).setTitle(title);
    loadBackdrop();
  }
  
  public boolean onCreateOptionsMenu(Menu paramMenu)
  {
    getMenuInflater().inflate(R.menu.sample_actions, paramMenu);
    return true;
  }
}
