package ng.healthfacts.myperfectweight;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

public class WeightGain
  extends AppCompatActivity
{
  private Toolbar mToolbar;
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setContentView(R.layout.activity_weight_gain);
    this.mToolbar = ((Toolbar)findViewById(R.id.toolbar));
    setSupportActionBar(this.mToolbar);
    getSupportActionBar().setDisplayHomeAsUpEnabled(true);
  }
}


/* WeightGain.class
 */