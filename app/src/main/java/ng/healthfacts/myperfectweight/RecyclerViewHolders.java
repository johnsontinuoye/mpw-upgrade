package ng.healthfacts.myperfectweight;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;
import ng.healthfacts.myperfectweight.gain.Day1;
import ng.healthfacts.myperfectweight.gain.Day10;
import ng.healthfacts.myperfectweight.gain.Day11;
import ng.healthfacts.myperfectweight.gain.Day12;
import ng.healthfacts.myperfectweight.gain.Day13;
import ng.healthfacts.myperfectweight.gain.Day14;
import ng.healthfacts.myperfectweight.gain.Day15;
import ng.healthfacts.myperfectweight.gain.Day16;
import ng.healthfacts.myperfectweight.gain.Day17;
import ng.healthfacts.myperfectweight.gain.Day18;
import ng.healthfacts.myperfectweight.gain.Day19;
import ng.healthfacts.myperfectweight.gain.Day2;
import ng.healthfacts.myperfectweight.gain.Day20;
import ng.healthfacts.myperfectweight.gain.Day21;
import ng.healthfacts.myperfectweight.gain.Day22;
import ng.healthfacts.myperfectweight.gain.Day23;
import ng.healthfacts.myperfectweight.gain.Day24;
import ng.healthfacts.myperfectweight.gain.Day25;
import ng.healthfacts.myperfectweight.gain.Day26;
import ng.healthfacts.myperfectweight.gain.Day27;
import ng.healthfacts.myperfectweight.gain.Day28;
import ng.healthfacts.myperfectweight.gain.Day29;
import ng.healthfacts.myperfectweight.gain.Day3;
import ng.healthfacts.myperfectweight.gain.Day30;
import ng.healthfacts.myperfectweight.gain.Day4;
import ng.healthfacts.myperfectweight.gain.Day5;
import ng.healthfacts.myperfectweight.gain.Day6;
import ng.healthfacts.myperfectweight.gain.Day7;
import ng.healthfacts.myperfectweight.gain.Day8;
import ng.healthfacts.myperfectweight.gain.Day9;

public class RecyclerViewHolders
  extends RecyclerView.ViewHolder
  implements View.OnClickListener
{
  public static final String Selection = "weightKey";
  public static final String mypreference = "mypref";
  public Context context;
  public TextView daytitle;
  SharedPreferences sharedpreferences;
  
  public RecyclerViewHolders(View paramView)
  {
    super(paramView);
    paramView.setOnClickListener(this);
    this.daytitle = ((TextView)paramView.findViewById(R.id.tvTitle));
  }
  
  public void onClick(View paramView)
  {
    switch (getPosition())
    {
    default: 
      this.context = paramView.getContext();
      this.context.startActivity(new Intent(this.context, Day30.class));
      return;
    case 0: 
      this.context = paramView.getContext();
      this.context.startActivity(new Intent(this.context, Day1.class));
      return;
    case 1: 
      this.context = paramView.getContext();
      this.context.startActivity(new Intent(this.context, Day2.class));
      return;
    case 2: 
      this.context = paramView.getContext();
      this.context.startActivity(new Intent(this.context, Day3.class));
      return;
    case 3: 
      this.context = paramView.getContext();
      this.context.startActivity(new Intent(this.context, Day4.class));
      return;
    case 4: 
      this.context = paramView.getContext();
      this.context.startActivity(new Intent(this.context, Day5.class));
      return;
    case 5: 
      this.context = paramView.getContext();
      this.context.startActivity(new Intent(this.context, Day6.class));
      return;
    case 6: 
      this.context = paramView.getContext();
      this.context.startActivity(new Intent(this.context, Day7.class));
      return;
    case 7: 
      this.context = paramView.getContext();
      this.context.startActivity(new Intent(this.context, Day8.class));
      return;
    case 8: 
      this.context = paramView.getContext();
      this.context.startActivity(new Intent(this.context, Day9.class));
      return;
    case 9: 
      this.context = paramView.getContext();
      this.context.startActivity(new Intent(this.context, Day10.class));
      return;
    case 10: 
      this.context = paramView.getContext();
      this.context.startActivity(new Intent(this.context, Day11.class));
      return;
    case 11: 
      this.context = paramView.getContext();
      this.context.startActivity(new Intent(this.context, Day12.class));
      return;
    case 12: 
      this.context = paramView.getContext();
      this.context.startActivity(new Intent(this.context, Day13.class));
      return;
    case 13: 
      this.context = paramView.getContext();
      this.context.startActivity(new Intent(this.context, Day14.class));
      return;
    case 14: 
      this.context = paramView.getContext();
      this.context.startActivity(new Intent(this.context, Day15.class));
      return;
    case 15: 
      this.context = paramView.getContext();
      this.context.startActivity(new Intent(this.context, Day16.class));
      return;
    case 16: 
      this.context = paramView.getContext();
      this.context.startActivity(new Intent(this.context, Day17.class));
      return;
    case 17: 
      this.context = paramView.getContext();
      this.context.startActivity(new Intent(this.context, Day18.class));
      return;
    case 18: 
      this.context = paramView.getContext();
      this.context.startActivity(new Intent(this.context, Day19.class));
      return;
    case 19: 
      this.context = paramView.getContext();
      this.context.startActivity(new Intent(this.context, Day20.class));
      return;
    case 20: 
      this.context = paramView.getContext();
      this.context.startActivity(new Intent(this.context, Day21.class));
      return;
    case 21: 
      this.context = paramView.getContext();
      this.context.startActivity(new Intent(this.context, Day22.class));
      return;
    case 22: 
      this.context = paramView.getContext();
      this.context.startActivity(new Intent(this.context, Day23.class));
      return;
    case 23: 
      this.context = paramView.getContext();
      this.context.startActivity(new Intent(this.context, Day24.class));
      return;
    case 24: 
      this.context = paramView.getContext();
      this.context.startActivity(new Intent(this.context, Day25.class));
      return;
    case 25: 
      this.context = paramView.getContext();
      this.context.startActivity(new Intent(this.context, Day26.class));
      return;
    case 26: 
      this.context = paramView.getContext();
      this.context.startActivity(new Intent(this.context, Day27.class));
      return;
    case 27: 
      this.context = paramView.getContext();
      this.context.startActivity(new Intent(this.context, Day28.class));
      return;
    case  28:
      this.context = paramView.getContext();
      this.context.startActivity(new Intent(this.context, Day29.class));
    }
  }
}


/* RecyclerViewHolders.class
 */