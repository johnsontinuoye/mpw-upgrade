package ng.healthfacts.myperfectweight;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.TextView;
import java.util.List;

public class RecyclerViewAdapterS
  extends RecyclerView.Adapter<RecyclerViewHoldersS>
{
  public Context context;
  public List<ItemObject> itemList;
  
  public RecyclerViewAdapterS(Context paramContext, List<ItemObject> paramList)
  {
    this.itemList = paramList;
    this.context = paramContext;
  }
  
  public int getItemCount()
  {
    return this.itemList.size();
  }
  
  public void onBindViewHolder(RecyclerViewHoldersS paramRecyclerViewHoldersS, int paramInt)
  {
    paramRecyclerViewHoldersS.daytitle.setText(((ItemObject)this.itemList.get(paramInt)).getDay());
  }
  
  public RecyclerViewHoldersS onCreateViewHolder(ViewGroup paramViewGroup, int paramInt)
  {
    return new RecyclerViewHoldersS(LayoutInflater.from(paramViewGroup.getContext()).inflate(R.layout.recyclerview_item_row, null));
  }
}


/* RecyclerViewAdapterS.class
 */