package ng.healthfacts.myperfectweight.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import ng.healthfacts.myperfectweight.R;
import ng.healthfacts.myperfectweight.model.SuggestedFood;
import ng.healthfacts.myperfectweight.model.Tables;

public class BackendFormActivity extends AppCompatActivity {

    private static final String TAG = BackendFormActivity.class.getSimpleName();
    Spinner wweightSelectionSpinner, daySelectionSpinner;
    private DatabaseReference foodSuggestionReference;
    private Button btnSubmitForm;
    private EditText etBreakfast, etLunch, etDinner, etSnack, etSnack2, etFruit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_backend_form);

        foodSuggestionReference = FirebaseDatabase.getInstance().getReference(Tables.FOOD_SUGGESTIONS);
        findViewById(R.id.btn_submit_form).setOnClickListener(this::submitForm);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setSupportActionBar(toolbar);
        setupActionBar();
        toolbar.setNavigationOnClickListener(v -> finish());

        wweightSelectionSpinner = (Spinner) findViewById(R.id.spinner_weight_selection);
        daySelectionSpinner = (Spinner) findViewById(R.id.spinner_day_selection);

        etBreakfast = (EditText) findViewById(R.id.et_breakfast);
        etLunch = (EditText) findViewById(R.id.et_lunch);
        etDinner = (EditText) findViewById(R.id.et_dinner);
        etSnack = (EditText) findViewById(R.id.et_snack);
        etSnack2 = (EditText) findViewById(R.id.et_snack2);
        etFruit = (EditText) findViewById(R.id.et_fruit);
        btnSubmitForm = (Button) findViewById(R.id.btn_submit_form);
    }

    private void submitForm(View view) {


        String day = (String)daySelectionSpinner.getSelectedItem();
        String breakfast = etBreakfast.getText().toString();
        String lunch = etLunch.getText().toString();
        String dinner = etDinner.getText().toString();
        String snack = etSnack.getText().toString();
        String snack2 = etSnack2.getText().toString();
        String fruit = etFruit.getText().toString();

        String weightSelected= (String)wweightSelectionSpinner.getSelectedItem();
        String weightType = "";
        if( weightSelected.toLowerCase().contains("loss") ) {
            if (weightSelected.toLowerCase().contains("fast")) {
                weightType = Tables.LOSS_FAST;//"loss_fast";
            }else if (weightSelected.toLowerCase().contains("slow")) {
                weightType = Tables.LOSS_SLOW;//"loss_slow";
            }
        }else if(weightSelected.toLowerCase().contains("gain")) {
            weightType = Tables.GAIN;//"gain";
        }


        Log.e(TAG, "submitForm: activity level "+breakfast );
        if(TextUtils.isEmpty(breakfast) || TextUtils.isEmpty(lunch)
                || TextUtils.isEmpty( dinner ) || TextUtils.isEmpty(snack)){
            Snackbar.make(view, "Please ensure that you fill the forms correctly.",
                    Snackbar.LENGTH_SHORT).show();
            return;
        }

        SuggestedFood suggestedFood = new SuggestedFood();
        suggestedFood.setBreakfast(breakfast);
        suggestedFood.setLunch(lunch);
        suggestedFood.setDinner(dinner);
        suggestedFood.setDay(day);
        suggestedFood.setSnack(snack);
        suggestedFood.setSnack2(snack2);
        suggestedFood.setFruit(fruit);
        String key = foodSuggestionReference.child(weightType).push().getKey();
        suggestedFood.setId(key);
        String finalWeightType = weightType;
        AlertDialog.Builder builder = new AlertDialog.Builder(this)
                .setTitle("Save this?")
                .setMessage("Is it safe to save this form? \n" +
                        "Choosing yes means you've verified that the information supplied is correct")
                .setPositiveButton("Yes", ((dialog, which) -> {
                    foodSuggestionReference.child(finalWeightType).child(key).setValue(suggestedFood, (databaseError, databaseReference) -> {
                        if (databaseError == null) { //no error
                            showSnackBar("Form saved successfully");
                            startActivity(new Intent(BackendFormActivity.this, BackendFormActivity.class));
                        } else {
                            showSnackBar("Cannot register user at this time");
                        }
                    });
                }))
                .setNegativeButton("No", ((dialog, which) -> {
                    dialog.dismiss();
                    showSnackBar("Form not saved");
                }));
        builder.create().show();



    }

    private void showSnackBar(String s) {
        Snackbar.make(btnSubmitForm, s,
                Snackbar.LENGTH_SHORT).show();
    }

    private void changeButtonToNext() {
        btnSubmitForm.setText(R.string.proceed);
        btnSubmitForm.setOnClickListener(view ->{
            startActivity( new Intent( this, DashboardActivity.class));
        });
    }

    private void saveFormDetails(String which, String key, String value){
        foodSuggestionReference.child(which).child(key).setValue(value);
    }
    private void setupActionBar() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            // Show the Up button in the action bar.
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            BackendFormActivity.this.finish();
            return true;
        }
        if (id == R.id.menu_view_backend) {
            startActivity( new Intent(this, BackendListActivity.class));
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_backend, menu);
        return true;
    }

}
