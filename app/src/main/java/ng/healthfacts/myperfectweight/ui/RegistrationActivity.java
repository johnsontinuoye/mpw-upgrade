package ng.healthfacts.myperfectweight.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import ng.healthfacts.myperfectweight.R;
import ng.healthfacts.myperfectweight.helper.AccountUtils;
import ng.healthfacts.myperfectweight.model.Tables;
import ng.healthfacts.myperfectweight.model.User;

public class RegistrationActivity extends AppCompatActivity {

    private static final String TAG = RegistrationActivity.class.getSimpleName();

    Spinner genderSpinner, weightUnitSpinner, 
            heightUnitSpinner, activitySpinner;
    private DatabaseReference userReference;

    Spinner weightSelectionSpinner, daySelectionSpinner;
    private DatabaseReference foodSuggestionReference;
    private Button btnSubmitProfile;
    private EditText etFirstName, etLastName, etAge, etHeight, etWeight;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        String userId = AccountUtils.getUserId(this);
        userReference = FirebaseDatabase.getInstance().getReference(Tables.USERS).child(userId);

        btnSubmitProfile = (Button) findViewById(R.id.btnSubmitProfile);
        btnSubmitProfile.setOnClickListener(this::submitProfile);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(v -> finish());
    
        genderSpinner = (Spinner) findViewById(R.id.spinner_gender);
        weightUnitSpinner = (Spinner) findViewById(R.id.spinnerWeightUnit);
        heightUnitSpinner = (Spinner) findViewById(R.id.spinner_height_unit);
        activitySpinner = (Spinner) findViewById(R.id.spinnerActivityLevel);

        etFirstName = (EditText) findViewById(R.id.et_first_name);
        etLastName = (EditText) findViewById(R.id.et_last_name);
        etAge = (EditText) findViewById(R.id.et_age);
        etHeight = (EditText) findViewById(R.id.et_height);
        etWeight = (EditText) findViewById(R.id.etWeight);

        User user;
        if( getIntent().hasExtra("USER")){
            user = (User) getIntent().getSerializableExtra("USER");

            String[] heightUnit = getResources().getStringArray(R.array.height_values);
            String[] weightUnit = getResources().getStringArray(R.array.weight_values);
            String[] genderUnit = getResources().getStringArray(R.array.gender_values);
            String[] activityLevel = getResources().getStringArray(R.array.activity_level);


            toolbar.setNavigationIcon(R.drawable.ic_arrow_back_24dp);
            toolbar.setNavigationOnClickListener(v -> finish());

            etFirstName.setText( user.getFirstName() );
            etLastName.setText( user.getLastName() );
            etAge.setText( String.valueOf(user.getAge()) );
            etHeight.setText( String.valueOf(user.getHeight() ));
            etWeight.setText( String.valueOf(user.getWeight() ));


            for( int i = 0; i < heightUnit.length; i++){
                if( heightUnit[i].equals(user.getHeightUnit()))
                    heightUnitSpinner.setSelection(i);
            }for( int i = 0; i < weightUnit.length; i++){
                if( weightUnit[i].equals(user.getWeightUnit()))
                    weightUnitSpinner.setSelection(i);
            }for( int i = 0; i < genderUnit.length; i++){
                if( genderUnit[i].equals(user.getGender()))
                    genderSpinner.setSelection(i);
            }for( int i = 0; i < activityLevel.length; i++){
                if( activityLevel[i].equals(user.getActivityLevel()))
                    activitySpinner.setSelection(i);
            }
        }

    }

    private void submitProfile(View view) {

        String firstName = etFirstName.getText().toString();
        String lastName = etLastName.getText().toString();
        String age = etAge.getText().toString();
        String height = etHeight.getText().toString();
        String weight = etWeight.getText().toString();
        String heightUnit = (String)heightUnitSpinner.getSelectedItem();
        String weightUnit = (String)weightUnitSpinner.getSelectedItem();
        String gender = (String) genderSpinner.getSelectedItem();
        String activityLevel = (String) activitySpinner.getSelectedItem();

        Log.e(TAG, "submitProfile: activity level "+activityLevel );
        if(TextUtils.isEmpty(firstName) || TextUtils.isEmpty(lastName)
                || TextUtils.isEmpty( age ) || TextUtils.isEmpty(height)){
            Snackbar.make(view, "Please ensure that you fill the forms correctly.",
                    Snackbar.LENGTH_SHORT).show();
            return;
        }

        if( TextUtils.isEmpty(heightUnit)
                || TextUtils.isEmpty(weightUnit)|| TextUtils.isEmpty(gender)){
            Snackbar.make(view, "Please ensure that you select the dropdown on the form.",
                    Snackbar.LENGTH_SHORT).show();
            return;
        }

        updateUserDetails("firstName", firstName);
        updateUserDetails("lastName", lastName);
        updateUserDetails("weightUnit", weightUnit);
        updateUserDetails("heightUnit", heightUnit);
        updateUserDetails("gender", gender);
        updateUserDetails("activityLevel", activityLevel);


        try {
            updateUserDetails(Integer.parseInt(age));
            updateUserDetails("height", Double.parseDouble(height));
            updateUserDetails("weight", Double.parseDouble(weight));

            changeButtonToNext();
            Snackbar.make(view, "Details saved successfully. ",
                    Snackbar.LENGTH_SHORT).show();
        }catch (NumberFormatException nfe){
            nfe.printStackTrace();
            Snackbar.make(view, "There are issues with saving this form. " +
                            "Please try again, ensuring the form is filled correctly. ",
                    Snackbar.LENGTH_SHORT).show();
        }
    }

    private int getWeightIndex(String[] weightTypes) {
        for( int i = 0; i < weightTypes.length; i++){
            if( weightTypes[i].contains("")){
                return i;
            }
        }
        return 0;
    }

    private void changeButtonToNext() {
        if( getIntent().hasExtra("USER")){
            btnSubmitProfile.setText(R.string.done);
            btnSubmitProfile.setOnClickListener(view ->{
                startActivity( new Intent( this, UserActivity.class));
            });
        } else {
            btnSubmitProfile.setText(R.string.proceed);
            btnSubmitProfile.setOnClickListener(view -> {
                startActivity(new Intent(this, DashboardActivity.class));

            });
        }
        finish();
    }

    private void updateUserDetails(String key, String value){
        userReference.child(key).setValue(value);/*, (OnCompleteListener<Task>) task -> {
            Log.e(TAG, "updateUserDetails: "+String.format("%s updated to %s", key, value) );
        });*/
    }
    private void updateUserDetails(int value){
        userReference.child("age").setValue(value);/*, (OnCompleteListener<Task>) task -> {
            Log.e(TAG, "updateUserDetails: "+String.format("age updated to %d", value) );
        });*/
    }
    private void updateUserDetails(String key, double value){
        userReference.child(key).setValue(value);/*, (OnCompleteListener<Task>) task -> {
            Log.e(TAG, "updateUserDetails: "+String.format("%s updated to %f", key, value) );
        });*/
    }
}
