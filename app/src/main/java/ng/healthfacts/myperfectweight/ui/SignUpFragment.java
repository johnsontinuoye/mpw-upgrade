package ng.healthfacts.myperfectweight.ui;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import ng.healthfacts.myperfectweight.R;
import ng.healthfacts.myperfectweight.helper.AccountUtils;
import ng.healthfacts.myperfectweight.helper.Utilities;
import ng.healthfacts.myperfectweight.model.User;

public class SignUpFragment extends Fragment {

    public static final String TAG = SignInFragment.class.getSimpleName();
    private OnFragmentInteractionListener mListener;
    private FirebaseAuth mAuth;
    EditText etUserName, etEmail, etPassword, etConfirmPassword;
    DatabaseReference userReference;
    private View btnSignIn;

    public SignUpFragment() {
        // Required empty public constructor
    }


    public static SignUpFragment newInstance(String param1, String param2) {
        SignUpFragment fragment = new SignUpFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_signup, container, false);

        mAuth = FirebaseAuth.getInstance();
        userReference = FirebaseDatabase.getInstance().getReference("users");
        etUserName = (EditText) view.findViewById(R.id.et_username);
        etEmail = (EditText) view.findViewById(R.id.et_email);
        etPassword = (EditText) view.findViewById(R.id.et_password);
        etConfirmPassword = (EditText) view.findViewById(R.id.et_confirm_password);
        Button signUpButton = view.findViewById(R.id.btnSignUp);
        TextWatcher passwordWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().length() < 8) {
                    etPassword.setError("Password should be at least 8 characters long");

                } else {
                    etPassword.setError(null);
                }

                if (TextUtils.isEmpty(etPassword.getText().toString())) {
                    etPassword.setError("Password field cannot be empty");
                } else {
                    etPassword.setError(null);
                }
            }
        };
        etPassword.addTextChangedListener(passwordWatcher);
        etConfirmPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(TextUtils.isEmpty(s.toString())){
                    etConfirmPassword.setError("Confirm password input");
                    //signUpButton.setEnabled(false);
                }else{
                    etConfirmPassword.setError(null);
                    //signUpButton.setEnabled(true);
                }

                if( !s.toString().equals(etPassword.getText().toString())){
                    etConfirmPassword.setError("Password does not match");
                    //signUpButton.setEnabled(false);
                }else{
                    etConfirmPassword.setError(null);
                    //signUpButton.setEnabled(true);
                }
            }
        });

        etUserName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if( s.toString().contains(" ")){
                    etUserName.setError("Username cannot contain space");
                }else {
                    etUserName.setError(null);
                }
            }
        });
        etEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(!Utilities.isGenuineEmailAddress(s.toString())){
                    etEmail.setError("This is not a valid email");
                } else etEmail.setError(null);
            }
        });
        signUpButton.setOnClickListener(v->{
                validateEntry();
        });

        etConfirmPassword.setOnEditorActionListener((v, actionId, event) -> {
            validateEntry();
            return true;
        });
        btnSignIn = view.findViewById(R.id.tvSignIn);
        btnSignIn.setOnClickListener(v->{
            SignInFragment si = new SignInFragment();
            getActivity().getSupportFragmentManager().beginTransaction()
                    .replace(R.id.login_frame , si)
                    .commit();
        });


        return view;
    }

    private void validateEntry() {
        View focusView = null;

        String email = etEmail.getText().toString();
        String username = etUserName.getText().toString();
        String password = etPassword.getText().toString();
        String cfPassword = etConfirmPassword.getText().toString();

        if( username.contains(" ")){
            focusView =etUserName;
        }
        if(!Utilities.isGenuineEmailAddress(email))
            focusView = etEmail;
        if( TextUtils.isEmpty(username)){
            focusView = etUserName;
        }
        if( TextUtils.isEmpty(password))
            focusView = etPassword;

        if( TextUtils.isEmpty(cfPassword))
            focusView = etConfirmPassword;

        if( focusView != null) {
            focusView.requestFocus();
        } else {
            createUserWithEmailAndPassword(username, email, password);
        }
    }

    private void createUserWithEmailAndPassword(String username, String email, String password) {

        User newUser = new User();
        String userKey = userReference.push().getKey();
        String userLogin = username+":"+password;
        String emailLogin = email+":"+password;
        newUser.setId(userKey);
        newUser.setUserName(username);
        newUser.setEmail(email);
        newUser.setPassword(password);

        User.Login login = new User.Login();
        login.setUserLogin(userLogin);
        login.setEmailLogin(emailLogin);
        newUser.setLogin( login );

        //Log.e(TAG, "createUserWithEmailAndPassword: user--> "+newUser );
        //userReference.push().setValue(newUser, (databaseError, databaseReference) -> {
        userReference.child(userKey).setValue(newUser, (databaseError, databaseReference) -> {
            if( databaseError == null ){ //no error
                Snackbar.make(btnSignIn, "Successfully registered.",
                        Snackbar.LENGTH_SHORT).show();
                AccountUtils.setUser(getActivity(), userKey, username, email, password);
                startActivity(new Intent(getActivity(), RegistrationActivity.class));
            }else{
                Toast.makeText(getActivity(), "Cannot register user at this time",
                        Toast.LENGTH_SHORT).show();
                Snackbar.make(btnSignIn, "Cannot register user at this time",
                        Snackbar.LENGTH_SHORT).show();
            }
        });
    }


    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }
}
