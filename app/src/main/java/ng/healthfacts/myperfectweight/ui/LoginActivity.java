package ng.healthfacts.myperfectweight.ui;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import ng.healthfacts.myperfectweight.R;
import ng.healthfacts.myperfectweight.helper.AccountUtils;

public class LoginActivity extends AppCompatActivity implements
        SignInFragment.OnFragmentInteractionListener,
        SignUpFragment.OnFragmentInteractionListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        SignInFragment loginFragment = new SignInFragment();
        getSupportFragmentManager().beginTransaction()
                .add(R.id.login_frame , loginFragment)
                .commit();
    }

    /*@Override
    protected void onStart() {
        super.onStart();

        if(hasLogin){
            //go to main activity
        }
        if( isFirstTime ){
            //show tutorial
            //show signup
        }
    }*/

    @Override
    public void onStart() {
        super.onStart();
        if(AccountUtils.hasLoggedIn(this)){
            startActivity( new Intent(this, DashboardActivity.class));
            finish();
            //startActivity( new Intent(this, RegistrationActivity.class));
        }
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
