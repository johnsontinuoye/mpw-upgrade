package ng.healthfacts.myperfectweight.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import net.cachapa.expandablelayout.ExpandableLayout;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.UUID;

import ng.healthfacts.myperfectweight.R;
import ng.healthfacts.myperfectweight.helper.AccountUtils;
import ng.healthfacts.myperfectweight.helper.Utilities;
import ng.healthfacts.myperfectweight.model.FoodClass;

public class DailyFoodActivity extends AppCompatActivity {

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = item -> {
        switch (item.getItemId()) {
            case R.id.navigation_dashboard:
                finish();
                return true;
            case R.id.navigation_daily_food:

                return true;
            case R.id.navigation_plan:
                startActivity(new Intent(DailyFoodActivity.this, PlanDetailsActivity.class));
                finish();
                return true;
            case R.id.navigation_user_details:
                startActivity(new Intent(DailyFoodActivity.this, UserActivity.class) );
                finish();
                return true;
        }
        return false;
    };


    private OnListFragmentInteractionListener mListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView( R.layout.activity_daily_food);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Daily Food Consumption");
        setSupportActionBar(toolbar);

        BottomNavigationView navigation = (BottomNavigationView)findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        navigation.setSelectedItemId(R.id.navigation_daily_food);

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMMM, yyyy", Locale.getDefault());
        String todayDateString = dateFormat.format(new Date());
        TextView tvCurrentDate = findViewById(R.id.tv_current_date);
        tvCurrentDate.setText(todayDateString);

        List<FoodClass> foodClassList = loadClassList();
        // Set the adapter

        /*RecyclerView recyclerView = (RecyclerView)findViewById(R.id.food_list);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        recyclerView.setAdapter(new FoodClassRecyclerViewAdapter(foodClassList, new OnListFragmentInteractionListener() {
            @Override
            public void onListFragmentInteraction(FoodClass item) {
                Log.e("DailyFoodActivity", "onListFragmentInteraction: item is "+item.getFoodClassName() );
            }
        }));*/
        Animation in = AnimationUtils.loadAnimation(this, android.R.anim.slide_in_left);
        Animation out = AnimationUtils.loadAnimation(this, android.R.anim.slide_out_right);
        LinearLayout listLayout = (LinearLayout) findViewById(R.id.list_layout);

        String[] lossList = {"Food", "Vegetables", "Fruits"};
        String[] gainList = {"Food", "Vegetables", "Fruits", "Snack"};

        String userPlan = AccountUtils.getUserPlan(this);
        if(!TextUtils.isEmpty(userPlan)) {
            if (userPlan.contains("loss")) {

                for (String item : lossList) {
                    View viewChild = getLayoutInflater().inflate(R.layout.layout_expandable_food, null);
                    ImageView imageView = (ImageView) viewChild.findViewById(R.id.image_switcher);
                    ExpandableLayout expandableLayout = (ExpandableLayout)
                            viewChild.findViewById(R.id.expandable_food_layout);
                    TextView tvHeader = (TextView) viewChild.findViewById(R.id.tv_header_text);
                    TextView tvHeaderTime = (TextView) viewChild.findViewById(R.id.tv_header_time_text);
                    TextView tvFoodDetails = (TextView) viewChild.findViewById(R.id.tv_food_details);

                    imageView.setImageResource(R.drawable.ic_add_black_24dp);

                    tvHeader.setText(item);
                    tvHeaderTime.setText("");
                    switch (item) {
                        case "Food":
                            tvFoodDetails.setText(Utilities.WEIGHT_LOSS_FOOD);
                            break;
                        case "Vegetables":
                            tvFoodDetails.setText(Utilities.WEIGHT_LOSS_VEGETABLE);
                            break;
                        case "Fruits":
                            tvFoodDetails.setText(Utilities.WEIGHT_LOSS_FRUIT);
                            break;
                    }
                    viewChild.setOnClickListener(v -> {
                        if (expandableLayout.isExpanded()) {
                            expandableLayout.collapse();
                            imageView.setImageResource(R.drawable.ic_add_black_24dp);
                            imageView.setAnimation(out);
                        } else {
                            expandableLayout.expand();
                            imageView.setImageResource(R.drawable.ic_remove_24dp);
                            imageView.setAnimation(in);
                        }
                    });
                    listLayout.addView(viewChild);
                }

            } else if (userPlan.contains("gain")) {
                for (String item : lossList) {
                    View viewChild = getLayoutInflater().inflate(R.layout.layout_expandable_food, null);
                    ImageView imageV = (ImageView) viewChild.findViewById(R.id.image_switcher);
                    ExpandableLayout expandableLayout = (ExpandableLayout)
                            viewChild.findViewById(R.id.expandable_food_layout);
                    TextView tvHeader = (TextView) viewChild.findViewById(R.id.tv_header_text);
                    TextView tvHeaderTime = (TextView) viewChild.findViewById(R.id.tv_header_time_text);
                    TextView tvFoodDetails = (TextView) viewChild.findViewById(R.id.tv_food_details);

                    Log.e("DFA", "onCreate: image is " + imageV.toString());
                    imageV.setImageResource(R.drawable.ic_add_black_24dp);

                    tvHeader.setText(item);
                    tvHeaderTime.setText("");
                    switch (item) {
                        case "Food":
                            tvFoodDetails.setText(Utilities.WEIGHT_GAIN_FOODS);
                            break;
                        case "Vegetables":
                            tvFoodDetails.setText(Utilities.WEIGHT_GAIN_VEGETABLES);
                            break;
                        case "Fruits":
                            tvFoodDetails.setText(Utilities.WEIGHT_GAIN_FRUITS);
                            break;
                        case "Snack":
                            tvFoodDetails.setText(Utilities.WEIGHT_GAIN_SNACK);
                            break;
                    }
                    viewChild.setOnClickListener(v -> {
                        if (expandableLayout.isExpanded()) {
                            expandableLayout.collapse();
                            imageV.setImageResource(R.drawable.ic_add_black_24dp);
                            imageV.setAnimation(out);
                        } else {
                            expandableLayout.expand();
                            imageV.setImageResource(R.drawable.ic_remove_24dp);
                            imageV.setAnimation(in);
                        }
                    });
                    listLayout.addView(viewChild);
                }
            }
        } else {
            TextView emptyTextView = new TextView(this);
            emptyTextView.setText(R.string.empty_plan_text);
            emptyTextView.setOnClickListener( v-> startActivity( new Intent(DailyFoodActivity.this, PlanSelectionActivity.class)));
            listLayout.addView(emptyTextView);
        }

    }

    private List<FoodClass> loadClassList() {
        List<FoodClass> foodClassList = new ArrayList<>();

        Calendar calendar = Calendar.getInstance();
        FoodClass foodA = new FoodClass("Food Items", UUID.randomUUID().toString(),34, calendar.getTimeInMillis());
        calendar.add(Calendar.MONTH, -1);
        FoodClass foodB = new FoodClass("Fruits", UUID.randomUUID().toString(),24, calendar.getTimeInMillis());
        calendar.add(Calendar.MONTH, -1);
        FoodClass foodC = new FoodClass("Vegetables", UUID.randomUUID().toString(),24, calendar.getTimeInMillis());

        foodClassList.add(foodA);
        foodClassList.add(foodB);
        foodClassList.add(foodC);

        String userPlan = AccountUtils.getUserPlan(this);
        String userPlanFull = "";
        if( userPlan.contains("loss")){
            if( userPlan.contains("fast")){
                userPlanFull = "Weight Loss (Fast)";
            }else {
                userPlanFull = "Weight Loss (Slow)";
            }
        } else if( userPlan.contains("gain")){
            if( userPlan.contains("fast")){
                userPlanFull = "Weight Gain (Fast)";
            } else {
                userPlanFull = "Weight Gain (Slow)";
            }
            FoodClass foodD = new FoodClass("Snacks", UUID.randomUUID().toString(),
                    24, calendar.getTimeInMillis());
            foodClassList.add(foodD);
        }
        return foodClassList;
    }



    public interface OnListFragmentInteractionListener {
        void onListFragmentInteraction(FoodClass item);
    }
    public void addFoodFromFab(View view) {
    }
}
