package ng.healthfacts.myperfectweight.ui;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import ng.healthfacts.myperfectweight.R;
import ng.healthfacts.myperfectweight.helper.AccountUtils;
import ng.healthfacts.myperfectweight.helper.Utilities;
import ng.healthfacts.myperfectweight.model.User;

import static ng.healthfacts.myperfectweight.helper.AccountUtils.setUser;

public class UserFragment extends Fragment {

    private OnFragmentInteractionListener mListener;
    private TextView tvUserWeight, tvUserWeightUnit, tvPresentDate, tvUserCalorie,
            tvUserName, tvUserHeight, tvUserAge;

    private RelativeLayout btnSeeAllRecords, btnLogOut;
    private ImageView userImage, editImage;
    View view;

    public UserFragment() {
        // Required empty public constructor
    }

    public static UserFragment newInstance(String param1, String param2) {
        UserFragment fragment = new UserFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_user, container, false);
        tvUserWeight = (TextView) view.findViewById(R.id.tv_user_weight);
        tvUserWeightUnit = (TextView) view.findViewById(R.id.tv_user_weight_unit);
        tvPresentDate = (TextView) view.findViewById(R.id.tv_present_date);
        tvUserCalorie = (TextView) view.findViewById(R.id.tv_user_calorie);
        tvUserName = (TextView) view.findViewById(R.id.tv_user_name);
        tvUserHeight = (TextView) view.findViewById(R.id.tv_user_height);
        tvUserAge = (TextView) view.findViewById(R.id.tv_user_age);
        //btnSeeAllRecords = (RelativeLayout) view.findViewById(R.id.btn_see_all_records);
        userImage = (ImageView) view.findViewById(R.id.iv_user_icon);
        btnLogOut = (RelativeLayout) view.findViewById(R.id.btn_log_out);
        btnLogOut.setOnClickListener(this::logOut);
        /*Toolbar toolbar = getActivity().findViewById(R.id.toolbar);
        toolbar.setTitle("My Profile");*/

        editImage = (ImageView) view.findViewById(R.id.iv_edit_user);
        editImage.setOnClickListener( v -> {
            AlertDialog alertDialog = new AlertDialog.Builder(getActivity())
                    .setTitle("Edit Info?")
                    .setMessage("Do you want to edit your profile?")
                    .setPositiveButton("Yes, Sure", ((dialog, which) -> {
                        editProfile();
                        dialog.dismiss();
                    }))
                    .setNeutralButton("Not now", ((dialog, which) -> dialog.dismiss()))
                    .create();
            alertDialog.show();
        });
        this.view = view;
        return view;
    }

    private void editProfile() {

        User user = AccountUtils.getUser(getActivity());

        Intent intent = new Intent(getActivity(), RegistrationActivity.class);
        intent.putExtra("USER", user);
        startActivity(intent);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        String userId = AccountUtils.getUserId(getActivity());
        DatabaseReference userReference = FirebaseDatabase.getInstance()
                .getReference("users").child(userId);

        userReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                User thisUser = dataSnapshot.getValue(User.class);
                updateUI(thisUser);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void updateUI(User user) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("EEEE, MMMM dd, yyyy", Locale.getDefault());
        tvUserWeight.setText(String.valueOf(user.getWeight()));
        tvUserWeightUnit.setText(user.getWeightUnit());
        tvPresentDate.setText(dateFormat.format(new Date()));
        double teeValue = Utilities.calculateTEE(user.getWeight(), user.getHeight(),
                user.getAge(), user.getActivityLevel());
        String display = String.valueOf(teeValue) +"Cal";
        tvUserCalorie.setText(display);

        tvUserName.setText(String.format("%s %s", user.getLastName(), user.getFirstName()));
        tvUserHeight.setText(String.format("%s %s", user.getHeight(), user.getHeightUnit()));
        tvUserAge.setText(String.valueOf(user.getAge()));

        setUser(getActivity(), user);
    }

    private void logOut(View view) {
        //ask for confirmation
        new AlertDialog.Builder(getActivity())
                .setTitle("Log Out")
                .setMessage("You are about to log out. Proceed with this action?")
                .setCancelable(false)
                .setNegativeButton("Cancel", ((dialog, which) -> dialog.dismiss()))
                .setPositiveButton("Log out", (dialog, which) -> {
                    //delete sharedpreferences
                    AccountUtils.deleteData(getActivity());
                    //clear activity stack and start login activity
                    Intent loginIntent = new Intent(getActivity(), LoginActivity.class);
                    loginIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(loginIntent);
                    getActivity().finish();
                })
                .create()
                .show();


    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }
}
