package ng.healthfacts.myperfectweight.ui;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import ng.healthfacts.myperfectweight.R;
import ng.healthfacts.myperfectweight.model.FoodClass;

public class DashboardActivity extends AppCompatActivity implements
        UserFragment.OnFragmentInteractionListener,
        DashboardFragment.OnFragmentInteractionListener,
        DailyFoodFragment.OnListFragmentInteractionListener{


    private static final String TAG = "DashboardActivity";
    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = item -> {
                switch (item.getItemId()) {
                    case R.id.navigation_dashboard:
//                        switchFragment( new DashboardFragment() );
                        return true;
                    case R.id.navigation_daily_food:
                        //switchFragment( new DailyFoodFragment() );
                        startActivity(new Intent(DashboardActivity.this, DailyFoodActivity.class));
                        return true;
                     case R.id.navigation_plan:
                        startActivity(new Intent(DashboardActivity.this, PlanDetailsActivity.class));
                        return true;
                    case R.id.navigation_user_details:
                        startActivity(new Intent(DashboardActivity.this, UserActivity.class));
                        return true;
                }
                return false;
            };
    private Toolbar toolbar;
    private BottomNavigationView navigation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

        navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        navigation.setSelectedItemId(R.id.navigation_dashboard);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.mipmap.ic_launcher);
        setSupportActionBar(toolbar);
        setTitle(R.string.app_name);

        getSupportFragmentManager().beginTransaction()
                .add(R.id.content, new DashboardFragment())
                .commit();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.e(TAG, "onResume: " );
        navigation.setSelectedItemId(R.id.navigation_dashboard);
    }

    private void switchFragment(Fragment fragment){
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.content, fragment)
                .commit();
    }
    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    public void onListFragmentInteraction(FoodClass item) {

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_dashboard, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
//        switch ( item.getItemId() ){
//            case R.id.menu_choose_plan:
//                startActivity(new Intent(this, PlanSelectionActivity.class));
//                /*if(TextUtils.isEmpty(AccountUtils.getUserPlan(this)))
//                    startActivity(new Intent(this, PlanSelectionActivity.class));
//                else
//                    startActivity(new Intent(this, PlanDetailsActivity.class));*/
//                break;
//        }
        return true;
    }
}
