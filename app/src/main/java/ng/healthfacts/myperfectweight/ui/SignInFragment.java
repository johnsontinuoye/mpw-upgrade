package ng.healthfacts.myperfectweight.ui;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import dmax.dialog.SpotsDialog;
import ng.healthfacts.myperfectweight.R;
import ng.healthfacts.myperfectweight.helper.AccountUtils;
import ng.healthfacts.myperfectweight.helper.Utilities;
import ng.healthfacts.myperfectweight.model.User;

public class SignInFragment extends Fragment {

    public static final String TAG = SignInFragment.class.getSimpleName();
    private OnFragmentInteractionListener mListener;
    private FirebaseAuth mAuth;
    EditText etEmail, etPassword;
    private DatabaseReference userNode;
    private View btnSignIn;
    private SpotsDialog dialog;

    public SignInFragment() {
        // Required empty public constructor
    }

    public static SignInFragment newInstance(String param1, String param2) {
        SignInFragment fragment = new SignInFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_signin, container, false);
        mAuth = FirebaseAuth.getInstance();
        userNode = FirebaseDatabase.getInstance().getReference("users");

        //dialog = new SpotsDialog(getActivity(), "Signing you in...");
        etEmail = (EditText) view.findViewById(R.id.et_email);
        etPassword = (EditText) view.findViewById(R.id.et_password);

        btnSignIn = view.findViewById(R.id.btnSignIn);
        btnSignIn.setOnClickListener(v->{
            signInUser();
           //startActivity( new Intent(getActivity(), DashboardActivity.class));
            // getActivity().finish();
        });

        view.findViewById(R.id.tvSignUp).setOnClickListener(v->{
            switchFragment(new SignUpFragment());
        });

        view.findViewById(R.id.tvForgotPassword).setOnClickListener(v->{

        });

        etPassword.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                signInUser();
                return true;
            }
        });
        return view;
    }

    private void signInUser() {
        String email = etEmail.getText().toString().trim(),
                password = etPassword.getText().toString();

        dialog.setCancelable(false);
        dialog.show();
        Log.e(TAG, "signInUser: email "+email );
        if(!Utilities.isGenuineEmailAddress(email)){
            //authenticate with username
            authWithUserName(email, password);
        } else {
            authWithEmail(email, password);
        }
    }

    private void stopDialog(){
        /*try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }*/
        for (int i = 0; i < 3000; i++) {
            System.out.println();
        }
        if( dialog != null ){
            dialog.dismiss();
        }
    }
    private void authWithEmail(String email, String password) {

        String toAuth = email+":"+password;
        Query query = userNode.orderByChild("login/emailLogin").equalTo(toAuth);
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    Log.e(TAG, "onDataChange: dataSnapshot -= "+dataSnapshot.toString() );
                    for( DataSnapshot snapshot: dataSnapshot.getChildren()) {
                        User loggedUser = snapshot.getValue(User.class);
                        if (loggedUser != null) {
                            AccountUtils.setUser(getActivity(),
                                    loggedUser.getId(), loggedUser.getUserName(), loggedUser.getEmail(), password);
                            AccountUtils.setUserPlan(getActivity(), loggedUser.getPlan());
                            AccountUtils.setUserPlanStartDate(getActivity(), loggedUser.getDatePlanStarted());
                            //TODO load user plan data
                            startActivity(new Intent(getActivity(), DashboardActivity.class));
                        }
                    }

                }else {
                    Snackbar.make(btnSignIn, "Incorrect credentials for "+email,
                            Snackbar.LENGTH_SHORT).show();
                }
                stopDialog();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(TAG, "onCancelled: error", databaseError.toException() );
                stopDialog();
                Snackbar.make(btnSignIn, "Cannot log you in at this time",
                        Snackbar.LENGTH_SHORT).show();
            }
        });
    }

    private void authWithUserName(String username, String password) {

        String toAuth = username+":"+password;
        Query query = userNode.orderByChild("login/userLogin").equalTo(toAuth);
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    Log.e(TAG, "onDataChange: "+ dataSnapshot.toString());

                    for( DataSnapshot snapshot: dataSnapshot.getChildren()) {
                        User loggedUser = snapshot.getValue(User.class);
                        if (loggedUser != null) {
                            AccountUtils.setUser(getActivity(),
                                    loggedUser.getId(), username, loggedUser.getEmail(), password);
                            startActivity(new Intent(getActivity(), DashboardActivity.class));
                        }
                    }
                }
                stopDialog();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                stopDialog();
                Snackbar.make(btnSignIn, "Cannot log you in at this time",
                        Snackbar.LENGTH_SHORT).show();
            }
        });
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    private void switchFragment(Fragment fragment){
        getActivity().getSupportFragmentManager().beginTransaction()
                .replace(R.id.login_frame, fragment)
                .commit();
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
