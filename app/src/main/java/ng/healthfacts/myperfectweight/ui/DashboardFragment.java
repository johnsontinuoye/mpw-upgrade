package ng.healthfacts.myperfectweight.ui;

import android.content.Context;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import net.cachapa.expandablelayout.ExpandableLayout;

import ng.healthfacts.myperfectweight.R;

public class DashboardFragment extends Fragment {

    private OnFragmentInteractionListener mListener;

    public DashboardFragment() {
        // Required empty public constructor
    }

    public static DashboardFragment newInstance(String param1, String param2) {
        DashboardFragment fragment = new DashboardFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_dashboard, container, false);

        Toolbar toolbar = getActivity().findViewById(R.id.toolbar);
        toolbar.setTitle(R.string.app_name);

        /*CardView welcomeNoteCard = (CardView) view.findViewById(R.id.cardview_welcome_note);
        view.findViewById(R.id.tv_read_more).setOnClickListener( v->{
            View layoutToHide = view.findViewById(R.id.layout_to_hide);
            if( layoutToHide.getVisibility() == View.VISIBLE){
                layoutToHide.setVisibility(View.GONE);
                view.findViewById(R.id.tv_read_more).setVisibility(View.VISIBLE);
            } else{
                layoutToHide.setVisibility(View.VISIBLE);
                view.findViewById(R.id.tv_read_more).setVisibility(View.GONE);
            }
        });*/
        //TextView tvWelcome = (TextView) view.findViewById(R.id.tv_welcome);
        //TextView tvWelcomeCont = (TextView) view.findViewById(R.id.tv_read_more_welcome);
        String welcomeNoteString = getResources().getString(R.string.welcome_note);
        String welcomeNoteContString = getResources().getString(R.string.welcome_note_long);
//        if( Build.VERSION.SDK_INT < Build.VERSION_CODES.N){
//            tvWelcome.setText(Html.fromHtml(welcomeNoteString));
//            tvWelcomeCont.setText(Html.fromHtml(welcomeNoteContString));
//        }else {
//            tvWelcome.setText(Html.fromHtml(welcomeNoteString, Html.FROM_HTML_MODE_COMPACT));
//            tvWelcomeCont.setText(Html.fromHtml(welcomeNoteContString, Html.FROM_HTML_MODE_COMPACT));
//        }
        /*SpannedString spannedString = new SpannedString(WELCOME_NOTE_CONT);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            tvWelcomeCont.setText(Html.fromHtml(WELCOME_NOTE_CONT, Html.FROM_HTML_MODE_LEGACY));
        }*/
//        ExpandableLayout expandableLayout = (ExpandableLayout) view.findViewById(R.id.expandable_overview_layout);
//        RelativeLayout relativeLayoutToHide = (RelativeLayout) view.findViewById(R.id.layout_to_hide);
//        TextView tvReadMore = (TextView) view.findViewById(R.id.tv_read_more);
//        relativeLayoutToHide.setOnClickListener( v -> {
//            if(expandableLayout.isExpanded()){
//                tvReadMore.setText(R.string.read_more);
//                expandableLayout.collapse();
//            } else {
//                tvReadMore.setText(R.string.read_less);
//                expandableLayout.expand();
//            }
//        });

        return view;
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }
}
