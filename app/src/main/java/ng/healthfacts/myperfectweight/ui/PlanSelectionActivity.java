package ng.healthfacts.myperfectweight.ui;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import devs.mulham.horizontalcalendar.HorizontalCalendar;
import devs.mulham.horizontalcalendar.HorizontalCalendarView;
import devs.mulham.horizontalcalendar.utils.HorizontalCalendarListener;
import ng.healthfacts.myperfectweight.R;
import ng.healthfacts.myperfectweight.db.SuggestedFoodViewModel;
import ng.healthfacts.myperfectweight.helper.AccountUtils;
import ng.healthfacts.myperfectweight.model.SuggestedFood;
import ng.healthfacts.myperfectweight.model.Tables;

public class PlanSelectionActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener{


    HorizontalCalendarView horizontalCalendarView;
    Spinner weightSelectionSpinner;
    public static final String TAG = PlanSelectionActivity.class.getSimpleName();
    String[] weightSelectionString;
    TextView tvHeader, tvBanner, tvContent;
    private DatabaseReference userReference;
    private DatabaseReference foodSuggestionReference;
    private SuggestedFoodViewModel suggestionViewModel;
    private Button btnJoin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_plan_selection);

        horizontalCalendarView = (HorizontalCalendarView)findViewById(R.id.calendarView);
        Calendar startDate = Calendar.getInstance();
        startDate.add(Calendar.MONTH, -1);

        Calendar endDate = Calendar.getInstance();
        startDate.add(Calendar.MONTH, 1);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        btnJoin = (Button) findViewById(R.id.btn_join);
        weightSelectionString = getResources().getStringArray(R.array.weight_selection_values);
        SimpleDateFormat dateFormat = new SimpleDateFormat("MMMM dd, yyyy", Locale.getDefault());
        HorizontalCalendar horizontalCalendar = new HorizontalCalendar.Builder(this, R.id.calendarView)
                .range(startDate, endDate)
                .datesNumberOnScreen(7)
                .build();
        horizontalCalendar.setCalendarListener(new HorizontalCalendarListener() {
            @Override
            public void onDateSelected(Calendar date, int position) {
                Log.e(TAG, "onDateSelected: date: "+dateFormat.format(date) );
            }

            @Override
            public void onCalendarScroll(HorizontalCalendarView calendarView, int dx, int dy) {
                super.onCalendarScroll(calendarView, dx, dy);
                Log.e(TAG, "onCalendarScroll: dx:dy="+dx+" : "+dy );
            }

            @Override
            public boolean onDateLongClicked(Calendar date, int position) {
                Log.e(TAG, "onCalendarScroll: date:position ="+date+" : "+position );
                return super.onDateLongClicked(date, position);
            }
        });

        weightSelectionSpinner = (Spinner) findViewById(R.id.spinner_weight_plan);
        weightSelectionSpinner.setOnItemSelectedListener( this );

        tvHeader = (TextView) findViewById(R.id.tv_header_text);
        tvBanner = (TextView) findViewById(R.id.tv_header_banner);
        tvContent = (TextView) findViewById(R.id.tv_content);


        String userId = AccountUtils.getUserId(this);
        userReference = FirebaseDatabase.getInstance().getReference(Tables.USERS).child(userId);
        foodSuggestionReference = FirebaseDatabase.getInstance().getReference(Tables.FOOD_SUGGESTIONS);

        suggestionViewModel = ViewModelProviders.of(this).get(SuggestedFoodViewModel.class);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                this.finish();
        }
        return true;
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        String selectedWeight = weightSelectionString[position];
        tvHeader.setText(selectedWeight);
        tvBanner.setText(selectedWeight);
        tvContent.setText(selectedWeight);

        //planBinding.btnJoin.setText( String.valueOf(planBinding.btnJoin.getText()+" "+selectedWeight));
        btnJoin.setOnClickListener(v -> onJoinButtonClicked(selectedWeight));
    }

    private void onJoinButtonClicked(String weightSelected) {
        Log.e(TAG, "onClick: selected weight "+weightSelected );
        String weightType = "";
        if( weightSelected.toLowerCase().contains("loss") ) {
            if (weightSelected.toLowerCase().contains("fast")) {
                weightType = Tables.LOSS_FAST;//"loss_fast";
            }else if (weightSelected.toLowerCase().contains("slow")) {
                weightType = Tables.LOSS_SLOW;//"loss_slow";
            }
        }else if(weightSelected.toLowerCase().contains("gain")) {
            weightType = Tables.GAIN;//"gain";
        }
        Date today = new Date();
        AccountUtils.setUserPlanStartDate(this, today);

        userReference.child("plan").setValue(weightType);
        userReference.child("datePlanStarted").setValue(today);

        foodSuggestionReference = FirebaseDatabase.getInstance()
                .getReference(Tables.FOOD_SUGGESTIONS);
        String userPlan = AccountUtils.getUserPlan(this);
        if(TextUtils.isEmpty(userPlan)){
            //User has not added any plan yet
            AccountUtils.setUserPlan(this, weightType);
        } else {
            //user has selected a plan before
        }
        String userId = AccountUtils.getUserId(this);
        userReference = FirebaseDatabase.getInstance().getReference(Tables.USERS).child(userId);

        foodSuggestionReference.child(userPlan).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Log.e(TAG, "onDataChange: snapshot 1 "+dataSnapshot.toString() );
                for( DataSnapshot snapshot: dataSnapshot.getChildren() ){
                    Log.e(TAG, "onDataChange: snapshot 2 "+snapshot.toString() );
                    SuggestedFood suggestedFood = snapshot.getValue( SuggestedFood.class );
                    suggestionViewModel.insertSingleSuggestedFood(suggestedFood);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        suggestionViewModel.getAllSuggestions().observe(this, suggestedFoods -> {
            if( suggestedFoods == null ){
                Log.e(TAG, "onJoinButtonClicked: size inserted is null == " );
                return;
            }

            Log.e(TAG, "onJoinButtonClicked: size inserted == "+suggestedFoods.size() );
        });
        AlertDialog alertDialog = new AlertDialog.Builder(this)
                .setTitle("Subscription")
                .setMessage("You have successfully subscribed to the "+weightSelected+ " plan. Welcome on board")
                .setPositiveButton("Alright", ((dialog, which) -> {
                    dialog.dismiss();
                    finish();
                }))
                .create();
        alertDialog.show();


        //startActivity( new Intent(this, PlanDetailsActivity.class));
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
