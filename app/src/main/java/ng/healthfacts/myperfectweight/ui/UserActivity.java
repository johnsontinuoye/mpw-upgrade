package ng.healthfacts.myperfectweight.ui;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import ng.healthfacts.myperfectweight.R;


public class UserActivity extends AppCompatActivity implements UserFragment.OnFragmentInteractionListener{

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = item -> {
        switch (item.getItemId()) {
            case R.id.navigation_dashboard:
                finish();
                return true;
            case R.id.navigation_daily_food:
                startActivity(new Intent(UserActivity.this, DailyFoodActivity.class) );
                finish();
                return true;
            case R.id.navigation_plan:
                startActivity(new Intent(UserActivity.this, PlanDetailsActivity.class));
                finish();
                return true;
            case R.id.navigation_user_details:
                return true;
        }
        return false;
    };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user);
        //ActivityUserBinding userBinding = DataBindingUtil.setContentView(this, R.layout.activity_user);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Me");
        toolbar.setSubtitle("My Profile");
        setSupportActionBar(toolbar);

        BottomNavigationView navigation = (BottomNavigationView)findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        navigation.setSelectedItemId(R.id.navigation_user_details);
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //getMenuInflater().inflate(R.menu.menu_user, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch ( item.getItemId() ){
            case R.id.menu_fill_user_backend:
                startActivity( new Intent(this, BackendFormActivity.class));
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
