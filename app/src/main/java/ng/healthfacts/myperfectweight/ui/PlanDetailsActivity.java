package ng.healthfacts.myperfectweight.ui;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.firebase.database.DatabaseReference;

import net.cachapa.expandablelayout.ExpandableLayout;

import java.util.Date;

import ng.healthfacts.myperfectweight.R;
import ng.healthfacts.myperfectweight.db.SuggestedFoodViewModel;
import ng.healthfacts.myperfectweight.helper.AccountUtils;
import ng.healthfacts.myperfectweight.helper.Utilities;
import ng.healthfacts.myperfectweight.model.SuggestedFood;

public class PlanDetailsActivity extends AppCompatActivity implements View.OnClickListener{

    public static final String TAG = PlanDetailsActivity.class.getSimpleName();

    TextView tvTodayText, tvBreakfastTime, tvLunchTime, tvDinnerTime, tvSnackTime, tvFruitTime;
    TextView tvBreakfastDetails, tvLunchDetails, tvDinnerDetails, tvSnackDetails, tvFruitDetails;

    LinearLayout foodItemLayout;
    LinearLayout breakfastItemsLayout, lunchItemsLayout, dinnerItemsLayout,
                fruitItemsLayout, snackItemsLayout;
    private DatabaseReference foodSuggestionReference;
    private DatabaseReference userReference;

    RelativeLayout relBreakfast,relLunch,relDinner,relSnack,relFruit;
    ExpandableLayout expBreakfast, expLunch, expDinner, expSnack, expFruit;

    ImageButton btnPrevious, btnNext;

    int currentDay = 0, otherDay=0;

    SuggestedFoodViewModel suggestedFoodViewModel;
    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = item -> {
        switch (item.getItemId()) {
            case R.id.navigation_dashboard:
                startActivity(new Intent(PlanDetailsActivity.this, DashboardActivity.class));
                finish();
                return true;
            case R.id.navigation_daily_food:
                startActivity(new Intent(PlanDetailsActivity.this, DailyFoodActivity.class));
                finish();
                return true;
            case R.id.navigation_plan:
                return true;
            case R.id.navigation_user_details:
                startActivity(new Intent(PlanDetailsActivity.this, UserActivity.class));
                finish();
                return true;
        }
        return false;
    };
    private Toolbar toolbar;
    private BottomNavigationView navigation;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_plan_details2);

        tvTodayText = (TextView) findViewById( R.id.tv_day_count);


         toolbar = (Toolbar) findViewById(R.id.toolbar);
         btnPrevious = (ImageButton) findViewById(R.id.btn_go_back);
         btnNext = (ImageButton) findViewById(R.id.btn_next);

        String userPlan = AccountUtils.getUserPlan(this);
        String userPlanFull = "";
        if( userPlan.contains("loss")){
            if( userPlan.contains("fast")){
                userPlanFull = "Weight Loss (Fast)";
            }else {
                userPlanFull = "Weight Loss (Slow)";
            }
        } else if( userPlan.contains("gain")){
            if( userPlan.contains("fast")){
                userPlanFull = "Weight Gain (Fast)";
            } else {
                userPlanFull = "Weight Gain (Slow)";
            }
        }

        toolbar.setTitle(userPlanFull);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(v->finish());

        navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        navigation.setSelectedItemId(R.id.navigation_plan);

        breakfastItemsLayout = (LinearLayout) findViewById(R.id.layout_breakfast_frame);
        lunchItemsLayout = (LinearLayout) findViewById(R.id.layout_lunch_frame);
        dinnerItemsLayout = (LinearLayout) findViewById(R.id.layout_dinner_frame);
        snackItemsLayout = (LinearLayout) findViewById(R.id.layout_snack_frame);
        fruitItemsLayout = (LinearLayout) findViewById(R.id.layout_fruit_frame);

//        relBreakfast = (RelativeLayout) findViewById(R.id.rel_breakfast);
//        relLunch = (RelativeLayout) findViewById(R.id.rel_lunch);
//        relDinner = (RelativeLayout) findViewById(R.id.rel_dinner);
//        relSnack = (RelativeLayout) findViewById(R.id.rel_snack);
//        relFruit = (RelativeLayout) findViewById(R.id.rel_fruit);
//
//        expBreakfast = (ExpandableLayout) findViewById(R.id.expandable_breakfast_layout);
//        expLunch = (ExpandableLayout) findViewById(R.id.expandable_lunch_layout);
//        expDinner = (ExpandableLayout) findViewById(R.id.expandable_dinner_layout);
//        expSnack = (ExpandableLayout) findViewById(R.id.expandable_snack_layout);
//        expFruit = (ExpandableLayout) findViewById(R.id.expandable_fruit_layout);
//
//        tvBreakfastDetails = (TextView) findViewById(R.id.tv_breakfast_details);
//        tvLunchDetails = (TextView) findViewById(R.id.tv_lunch_details);
//        tvDinnerDetails = (TextView) findViewById(R.id.tv_dinner_details);
//        tvSnackDetails = (TextView) findViewById(R.id.tv_snack_details);
//        tvFruitDetails = (TextView) findViewById(R.id.tv_fruit_details);

        tvBreakfastTime = (TextView) findViewById(R.id.tv_breakfast_time_text);
        tvLunchTime = (TextView) findViewById(R.id.tv_lunch_time_text);


        btnNext.setOnClickListener(this);
        btnPrevious.setOnClickListener(this);
        loadCurrentDayData();

//        relBreakfast.setOnClickListener( v -> {
//            if( expBreakfast.isExpanded()) expBreakfast.collapse();
//            else expBreakfast.expand();
//        });
//        relLunch.setOnClickListener( v -> {
//            if( expLunch.isExpanded()) expLunch.collapse();
//            else expLunch.expand();
//        });
//        relDinner.setOnClickListener( v -> {
//            if( expDinner.isExpanded()) expDinner.collapse();
//            else expDinner.expand();
//        });
//        relSnack.setOnClickListener( v -> {
//            if( expSnack.isExpanded()) expSnack.collapse();
//            else expSnack.expand();
//        });
//        relFruit.setOnClickListener( v -> {
//            if( expFruit.isExpanded()) expFruit.collapse();
//            else expFruit.expand();
//        });


    }

    private void loadCurrentDayData() {
        suggestedFoodViewModel = ViewModelProviders.of(this).get(SuggestedFoodViewModel.class);

        suggestedFoodViewModel.getAllSuggestions().observe(this, suggestedFoods -> {
            if( suggestedFoods == null ) {
                findViewById(R.id.linear_layout).setVisibility(View.GONE);
                findViewById(R.id.tv_empty_plan).setVisibility(View.VISIBLE);
                return;
            }
            SuggestedFood todayFood = new SuggestedFood(),
                    otherDayFood = new SuggestedFood();
            for(SuggestedFood food: suggestedFoods){

                String day = food.getDay();
                Date userPlanStarted = AccountUtils.getUserPlanStartDate(PlanDetailsActivity.this);
                currentDay = Utilities.diffInDays(userPlanStarted);
                currentDay = currentDay + 1;
                Log.e(TAG, "loadCurrentDayData: diff day == "+currentDay );

                //currentDay = 3;
                otherDay = currentDay + 7;
                if( otherDay > 30 ){
                    otherDay = otherDay - 30;
                }

                if( Integer.parseInt(day) == currentDay ){
                    todayFood = food;
                }
                if( Integer.parseInt(day) == otherDay ){
                    otherDayFood = food;
                }
            }
            workWithCurrentDayData(todayFood, otherDayFood, currentDay);
        });
    }

    private void loadNextDayData(int currentDay) {
        suggestedFoodViewModel = ViewModelProviders.of(this).get(SuggestedFoodViewModel.class);

        suggestedFoodViewModel.getAllSuggestions().observe(this, suggestedFoods -> {
            if( suggestedFoods == null ) {
                findViewById(R.id.linear_layout).setVisibility(View.GONE);
                findViewById(R.id.tv_empty_plan).setVisibility(View.VISIBLE);
                return;
            }
            SuggestedFood todayFood = new SuggestedFood(),
                    otherDayFood = new SuggestedFood();
            for(SuggestedFood food: suggestedFoods){

                String day = food.getDay();
                Date userPlanStarted = AccountUtils.getUserPlanStartDate(PlanDetailsActivity.this);

                Log.e(TAG, "loadNextDayData: diff day == "+currentDay );

                //currentDay = 3;
                otherDay = currentDay + 7;
                if( otherDay > 30 ){
                    otherDay = otherDay - 30;
                }

                if( Integer.parseInt(day) == currentDay ){
                    todayFood = food;
                }
                if( Integer.parseInt(day) == otherDay ){
                    otherDayFood = food;
                }
            }
            workWithCurrentDayData(todayFood, otherDayFood, currentDay);
        });
    }

    private void workWithCurrentDayData(SuggestedFood food, SuggestedFood otherDayFood, int currentDay) {

        breakfastItemsLayout = (LinearLayout) findViewById(R.id.layout_breakfast_frame);
        lunchItemsLayout = (LinearLayout) findViewById(R.id.layout_lunch_frame);
        dinnerItemsLayout = (LinearLayout) findViewById(R.id.layout_dinner_frame);
        snackItemsLayout = (LinearLayout) findViewById(R.id.layout_snack_frame);
        fruitItemsLayout = (LinearLayout) findViewById(R.id.layout_fruit_frame);

        //tvTodayText.setText(String.format("Day %s", currentDay));
        tvTodayText.setText(String.format("%s", currentDay));

        Log.e(TAG, "workWithCurrentDayData: Food suggested == "+food );
        Log.e(TAG, "workWithCurrentDayData: Food suggested next == "+otherDayFood );
        String breakfast = food.getBreakfast();
        String lunch = food.getLunch();
        String dinner = food.getDinner();
        String snack = food.getSnack();
        String snack2 = food.getSnack2();

        if( breakfast == null || lunch == null || dinner == null || snack == null ) {
            findViewById(R.id.tv_empty_plan).setVisibility(View.VISIBLE);
            findViewById(R.id.scroll_view).setVisibility(View.GONE);
            return;
        }
        //if( snack != null)
        snack = snack.concat("&"+snack2);
        String fruit = food.getFruit();

        String[] breakfastMeals = ("Option A: &".concat(breakfast)).split("&");
        String[] lunchMeals = ("Option A: &".concat(lunch)).split("&");
        String[] dinnerMeals = ("Option A: &".concat(dinner)).split("&");
        String[] snackMeals = ("Option A: &".concat(snack)).split("&");
        String[] fruitMeals = new String[0];
        if (fruit != null) {
            fruitMeals = ("Option A: &".concat(fruit)).split("&");
        }


        String breakfast1 = otherDayFood.getBreakfast();
        String lunch1 = otherDayFood.getLunch();
        String dinner1 = otherDayFood.getDinner();
        String snack1 = otherDayFood.getSnack();
        String snack21 = otherDayFood.getSnack2();

        if( snack1 != null )
            snack1 = snack1.concat("&"+snack21);
        String fruit1 = otherDayFood.getFruit();

        String[] breakfastMeals1 = ("Option B: &".concat(breakfast1)).split("&");
        String[] lunchMeals1 = ("Option B: &".concat(lunch1)).split("&");
        String[] dinnerMeals1 = ("Option B: &".concat(dinner1)).split("&");
        String[] snackMeals1 = ("Option B: &".concat(snack1)).split("&");
        String[] fruitMeals1 = new String[0];
        if (fruit1 != null) {
            fruitMeals1 = ("Option B: &".concat(fruit1)).split("&");
        }

        View viewChild;
        TextView tvListItemFoodName;

        for( String string: breakfastMeals){
            viewChild = getLayoutInflater().inflate(R.layout.layout_checkbox_item, null);
            tvListItemFoodName = (TextView) viewChild.findViewById(R.id.tv_list_item_food_name);
            if(!TextUtils.isEmpty(string)) {
                if( string.equals( breakfastMeals[0])){
                    tvListItemFoodName.setTypeface(null, Typeface.BOLD);
                    tvListItemFoodName.setText("\n".concat(string.trim()));
                    breakfastItemsLayout.addView(viewChild);
                } else{
                    tvListItemFoodName.setTypeface(tvListItemFoodName.getTypeface(), Typeface.NORMAL);
                    tvListItemFoodName.setText(string.trim());
                    breakfastItemsLayout.addView(viewChild);
                }
            }
        }

        for( String string: breakfastMeals1){
            viewChild = getLayoutInflater().inflate(R.layout.layout_checkbox_item, null);
            tvListItemFoodName = (TextView) viewChild.findViewById(R.id.tv_list_item_food_name);
            if(!TextUtils.isEmpty(string)) {
                if( string.equals( breakfastMeals1[0])){
                    tvListItemFoodName.setTypeface(null, Typeface.BOLD);
                    tvListItemFoodName.setText("\n".concat(string.trim()));
                    breakfastItemsLayout.addView(viewChild);
                } else {
                    tvListItemFoodName.setTypeface(tvListItemFoodName.getTypeface(), Typeface.NORMAL);
                    tvListItemFoodName.setText(string.trim());
                    breakfastItemsLayout.addView(viewChild);
                }
            }
        }

        for( String string: lunchMeals){
            viewChild = getLayoutInflater().inflate(R.layout.layout_checkbox_item, null);
            tvListItemFoodName = (TextView) viewChild.findViewById(R.id.tv_list_item_food_name);
            if(!TextUtils.isEmpty(string)) {
                if( string.equals( lunchMeals[0])){
                    tvListItemFoodName.setTypeface(null, Typeface.BOLD);
                    tvListItemFoodName.setText("\n".concat(string.trim()));
                    lunchItemsLayout.addView(viewChild);
                } else {
                    tvListItemFoodName.setTypeface(tvListItemFoodName.getTypeface(), Typeface.NORMAL);
                    tvListItemFoodName.setText(string.trim());
                    lunchItemsLayout.addView(viewChild);
                }
            }
        }
        for( String string: lunchMeals1){
            viewChild = getLayoutInflater().inflate(R.layout.layout_checkbox_item, null);
            tvListItemFoodName = (TextView) viewChild.findViewById(R.id.tv_list_item_food_name);
            if(!TextUtils.isEmpty(string)) {
                if( string.equals( lunchMeals1[0])){
                    tvListItemFoodName.setTypeface(null, Typeface.BOLD);
                    tvListItemFoodName.setText("\n".concat(string.trim()));
                    lunchItemsLayout.addView(viewChild);
                } else {
                    tvListItemFoodName.setTypeface(tvListItemFoodName.getTypeface(), Typeface.NORMAL);
                    tvListItemFoodName.setText(string.trim());
                    lunchItemsLayout.addView(viewChild);
                }
            }
        }

        for( String string: dinnerMeals){
            viewChild = getLayoutInflater().inflate(R.layout.layout_checkbox_item, null);
            tvListItemFoodName = (TextView) viewChild.findViewById(R.id.tv_list_item_food_name);
            if(!TextUtils.isEmpty(string)) {
                if( string.equals( dinnerMeals[0])){
                    tvListItemFoodName.setTypeface(null, Typeface.BOLD);
                    tvListItemFoodName.setText("\n".concat(string.trim()));
                    dinnerItemsLayout.addView(viewChild);
                } else {
                    tvListItemFoodName.setTypeface(tvListItemFoodName.getTypeface(), Typeface.NORMAL);
                    tvListItemFoodName.setText(string.trim());
                    dinnerItemsLayout.addView(viewChild);
                }
            }
        }
        for( String string: dinnerMeals1){
            viewChild = getLayoutInflater().inflate(R.layout.layout_checkbox_item, null);
            tvListItemFoodName = (TextView) viewChild.findViewById(R.id.tv_list_item_food_name);
            if(!TextUtils.isEmpty(string)) {
                if( string.equals( dinnerMeals1[0])){
                    tvListItemFoodName.setTypeface(null, Typeface.BOLD);
                    tvListItemFoodName.setText("\n".concat(string.trim()));
                    dinnerItemsLayout.addView(viewChild);
                } else {
                    tvListItemFoodName.setTypeface(tvListItemFoodName.getTypeface(), Typeface.NORMAL);
                    tvListItemFoodName.setText(string.trim());
                    dinnerItemsLayout.addView(viewChild);
                }
            }
        }

        for( String string: snackMeals){
            viewChild = getLayoutInflater().inflate(R.layout.layout_checkbox_item, null);
            tvListItemFoodName = (TextView) viewChild.findViewById(R.id.tv_list_item_food_name);
            if(!TextUtils.isEmpty(string)) {
                if( string.equals( snackMeals[0])){
                    tvListItemFoodName.setTypeface(null, Typeface.BOLD);
                    tvListItemFoodName.setText("\n".concat(string.trim()));
                    snackItemsLayout.addView(viewChild);
                } else {
                    tvListItemFoodName.setTypeface(tvListItemFoodName.getTypeface(), Typeface.NORMAL);
                    tvListItemFoodName.setText(string.trim());
                    snackItemsLayout.addView(viewChild);
                }
            }
        }

        for( String string: snackMeals1){
            viewChild = getLayoutInflater().inflate(R.layout.layout_checkbox_item, null);
            tvListItemFoodName = (TextView) viewChild.findViewById(R.id.tv_list_item_food_name);
            if(!TextUtils.isEmpty(string)) {
                if( string.equals( snackMeals1[0])){
                    tvListItemFoodName.setTypeface(null, Typeface.BOLD);
                    tvListItemFoodName.setText("\n".concat(string.trim()));
                    snackItemsLayout.addView(viewChild);
                } else {
                    tvListItemFoodName.setTypeface(tvListItemFoodName.getTypeface(), Typeface.NORMAL);
                    tvListItemFoodName.setText(string.trim());
                    snackItemsLayout.addView(viewChild);
                }
            }
        }

        if( fruitMeals.length > 1 ) {
            for (String string : fruitMeals) {
                viewChild = getLayoutInflater().inflate(R.layout.layout_checkbox_item, null);
                tvListItemFoodName = (TextView) viewChild.findViewById(R.id.tv_list_item_food_name);
                if (!TextUtils.isEmpty(string)) {
                    if (string.equals(fruitMeals[0])) {
                        tvListItemFoodName.setTypeface(null, Typeface.BOLD);
                        tvListItemFoodName.setText("\n".concat(string.trim()));
                        fruitItemsLayout.addView(viewChild);
                    } else {
                        tvListItemFoodName.setTypeface(tvListItemFoodName.getTypeface(), Typeface.NORMAL);
                        tvListItemFoodName.setText(string.trim());
                        fruitItemsLayout.addView(viewChild);
                    }
                }
            }
        }
        if( fruitMeals1.length > 1 ) {
            for (String string : fruitMeals1) {
                viewChild = getLayoutInflater().inflate(R.layout.layout_checkbox_item, null);
                tvListItemFoodName = (TextView) viewChild.findViewById(R.id.tv_list_item_food_name);
                if (!TextUtils.isEmpty(string)) {
                    if (string.equals(fruitMeals1[0])) {
                        tvListItemFoodName.setTypeface(null, Typeface.BOLD);
                        tvListItemFoodName.setText("\n".concat(string.trim()));
                        fruitItemsLayout.addView(viewChild);
                    } else {
                        tvListItemFoodName.setTypeface(tvListItemFoodName.getTypeface(), Typeface.NORMAL);
                        tvListItemFoodName.setText(string.trim());
                        fruitItemsLayout.addView(viewChild);
                    }
                }
            }
        }


        findViewById(R.id.tv_empty_plan).setVisibility(View.GONE);
    }

    private void clearViews(){
        if( ((LinearLayout) breakfastItemsLayout).getChildCount() > 0)
            breakfastItemsLayout.removeAllViews();
        lunchItemsLayout.removeAllViews();
        dinnerItemsLayout.removeAllViews();
        snackItemsLayout.removeAllViews();
        fruitItemsLayout.removeAllViews();
    }
    @Override
    public void onClick(View v) {
        switch( v.getId() ){
            case R.id.btn_go_back:
                clearViews();
                currentDay = currentDay - 1;
                if( currentDay > 0) {
                    loadNextDayData(currentDay);
                }
                break;
            case R.id.btn_next:
                clearViews();
                currentDay = currentDay + 1;
                if( currentDay >= 0 && currentDay < 30)
                    loadNextDayData(currentDay);
                break;
        }
    }
}
