package ng.healthfacts.myperfectweight.ui;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import ng.healthfacts.myperfectweight.R;
import ng.healthfacts.myperfectweight.model.SuggestedFood;
import ng.healthfacts.myperfectweight.model.Tables;

public class EditSuggestedFoodFragment extends DialogFragment {

    private static final String FOOD = "FOOD";
    private static final String WEIGHT = "WEIGHT";

    private SuggestedFood suggestedFood;
    private String weightGroup;
    private EditText etBreakfast, etLunch, etDinner, etSnack, etSnack2, etFruit;

    Spinner weightSelectionSpinner, daySelectionSpinner;
    private DatabaseReference foodSuggestionReference;
    private Button btnSubmitForm;

    public EditSuggestedFoodFragment() {
        // Required empty public constructor
    }

    public static EditSuggestedFoodFragment newInstance(String param1, String param2) {
        EditSuggestedFoodFragment fragment = new EditSuggestedFoodFragment();
        Bundle args = new Bundle();
        args.putString(FOOD, param1);
        args.putString(WEIGHT, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            suggestedFood = (SuggestedFood) getArguments().getSerializable(FOOD);
            weightGroup = getArguments().getString(WEIGHT);
        }
    }

    /*@NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
        layoutParams.copyFrom(getDialog().getWindow().getAttributes());

        layoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
        layoutParams.height = WindowManager.LayoutParams.MATCH_PARENT;

        getDialog().getWindow().setAttributes( layoutParams);
        return getDialog();
    }*/

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_edit_suggested_food, container, false);
        foodSuggestionReference = FirebaseDatabase.getInstance().getReference(Tables.FOOD_SUGGESTIONS);

        Toolbar toolbar = (Toolbar) view.findViewById(R.id.toolbar);
        toolbar.setTitle("Edit suggestion");
        toolbar.setNavigationIcon(R.drawable.ic_close_black_24dp);
        toolbar.setNavigationOnClickListener(v -> dismiss());

        btnSubmitForm = (Button) view.findViewById(R.id.btn_submit_form);
        btnSubmitForm.setOnClickListener(this::submitForm);

        weightSelectionSpinner = (Spinner) view.findViewById(R.id.spinner_weight_selection);
        daySelectionSpinner = (Spinner) view.findViewById(R.id.spinner_day_selection);

        weightSelectionSpinner.setFocusable(false);
        weightSelectionSpinner.setEnabled(false);

        etBreakfast = (EditText) view.findViewById(R.id.et_breakfast);
        etLunch = (EditText) view.findViewById(R.id.et_lunch);
        etDinner = (EditText) view.findViewById(R.id.et_dinner);
        etSnack = (EditText) view.findViewById(R.id.et_snack);
        etSnack2 = (EditText) view.findViewById(R.id.et_snack2);
        etFruit = (EditText) view.findViewById(R.id.et_fruit);

        if( suggestedFood != null ) {
            //set values
            String[] weightTypes = getResources().getStringArray(R.array.weight_selection_values);
            weightSelectionSpinner.setSelection( getWeightIndex(weightTypes) );
            daySelectionSpinner.setSelection( Integer.parseInt(suggestedFood.getDay()));

            etBreakfast.setText(suggestedFood.getBreakfast());
            etLunch.setText(suggestedFood.getLunch());
            etDinner.setText(suggestedFood.getDinner());
            etSnack.setText(suggestedFood.getSnack() != null ? suggestedFood.getSnack(): "");
            etSnack2.setText(suggestedFood.getSnack2() != null ? suggestedFood.getSnack2(): " ");
            etFruit.setText(suggestedFood.getFruit() != null ? suggestedFood.getFruit(): " ");
        }
        return view;
    }

    private int getWeightIndex(String[] weightTypes) {
        for( int i = 0; i < weightTypes.length; i++){
            if( weightTypes[i].contains(weightGroup)){
                return i;
            }
        }
        return 0;
    }

    public void onButtonPressed(Uri uri) {
        /*if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }*/
    }


    @Override
    public void onStart() {
        super.onStart();
        if(getDialog() != null ) {
            getDialog()
                    .getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,
                    WindowManager.LayoutParams.WRAP_CONTENT);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        /*if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }*/
    }

    @Override
    public void onDetach() {
        super.onDetach();
        //mListener = null;
    }

    /*
    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }*/

    private void submitForm(View view) {


        String day = (String)daySelectionSpinner.getSelectedItem();

        String breakfast = etBreakfast.getText().toString();
        String lunch = etLunch.getText().toString();
        String dinner = etDinner.getText().toString();
        String snack = etSnack.getText().toString();
        String snack2 = etSnack2.getText().toString();
        String fruit = etFruit.getText().toString();

        String weightSelected= (String)weightSelectionSpinner.getSelectedItem();
        String weightType = "";
        if( weightSelected.toLowerCase().contains("loss") ) {
            if (weightSelected.toLowerCase().contains("fast")) {
                weightType = Tables.LOSS_FAST;//"loss_fast";
            }else if (weightSelected.toLowerCase().contains("slow")) {
                weightType = Tables.LOSS_SLOW;//"loss_slow";
            }
        }else if(weightSelected.toLowerCase().contains("gain")) {
            weightType = Tables.GAIN;//"gain";
        }


        Log.e("EditSuggestion", "submitForm: activity level "+breakfast );
        if(TextUtils.isEmpty(breakfast) || TextUtils.isEmpty(lunch)
                || TextUtils.isEmpty( dinner ) || TextUtils.isEmpty(snack)){
           showSnackBar( "Please ensure that you fill the forms correctly.");
            return;
        }

        SuggestedFood suggestedFood = new SuggestedFood();
        suggestedFood.setBreakfast(breakfast);
        suggestedFood.setLunch(lunch);
        suggestedFood.setDinner(dinner);
        suggestedFood.setDay(day);
        suggestedFood.setSnack(snack);
        suggestedFood.setSnack2(snack2);
        suggestedFood.setFruit(fruit);
        //String key = foodSuggestionReference.child(weightType).push().getKey();
        suggestedFood.setId( this.suggestedFood.getId() );
        String finalWeightType = weightType;
        Log.e("ESFF", "submitForm: "+this.suggestedFood );
        Log.e("ESFF", "submitForm: "+suggestedFood );
        Log.e("ESFF", "submitForm: "+finalWeightType );
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity())
                .setTitle("Save this?")
                .setMessage("Is it safe to save this edit? \n" +
                        "Choosing yes means you've verified that the information supplied is correct")
                .setPositiveButton("Yes", ((dialog, which) -> {
                    foodSuggestionReference.child(finalWeightType).child(suggestedFood.getId())
                            .setValue(suggestedFood, (databaseError, databaseReference) -> {
                        if (databaseError == null) { //no error
                            showSnackBar("Form saved successfully");
                            getDialog().dismiss();
                        } else {
                            showSnackBar("Cannot register user at this time");
                        }
                        dialog.dismiss();

                    });
                }))
                .setNegativeButton("No", ((dialog, which) -> {
                    dialog.dismiss();
                    showSnackBar("Form not saved");
                }));
        builder.create().show();



    }

    private void showSnackBar(String s) {
        Snackbar.make(btnSubmitForm, s,
                Snackbar.LENGTH_SHORT).show();
    }
}
