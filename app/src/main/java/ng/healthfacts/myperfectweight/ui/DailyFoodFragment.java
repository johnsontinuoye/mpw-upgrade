package ng.healthfacts.myperfectweight.ui;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.UUID;

import ng.healthfacts.myperfectweight.R;
import ng.healthfacts.myperfectweight.model.FoodClass;

public class DailyFoodFragment extends Fragment {

    private static final String ARG_COLUMN_COUNT = "column-count";
    private int mColumnCount = 1;
    private OnListFragmentInteractionListener mListener;

    public DailyFoodFragment() {
    }

    @SuppressWarnings("unused")
    public static DailyFoodFragment newInstance(int columnCount) {
        DailyFoodFragment fragment = new DailyFoodFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_COLUMN_COUNT, columnCount);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mColumnCount = getArguments().getInt(ARG_COLUMN_COUNT);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_daily_food, container, false);

        List<FoodClass> foodClassList = loadClassList();
        // Set the adapter
        if (view instanceof RecyclerView) {
            Context context = view.getContext();
            RecyclerView recyclerView = (RecyclerView) view;
            if (mColumnCount <= 1) {
                recyclerView.setLayoutManager(new LinearLayoutManager(context));
            } else {
                recyclerView.setLayoutManager(new GridLayoutManager(context, mColumnCount));
            }
            //recyclerView.setAdapter(new FoodClassRecyclerViewAdapter(foodClassList, mListener));
        }

        Toolbar toolbar = getActivity().findViewById(R.id.toolbar);
        toolbar.setTitle("Daily Food");
        return view;
    }

    private List<FoodClass> loadClassList() {
        List<FoodClass> foodClassList = new ArrayList<>();

        Calendar calendar = Calendar.getInstance();
        FoodClass foodA = new FoodClass("Protein", UUID.randomUUID().toString(),34, calendar.getTimeInMillis());
        calendar.add(Calendar.MONTH, -1);
        FoodClass foodB = new FoodClass("Fats and Oils", UUID.randomUUID().toString(),24, calendar.getTimeInMillis());
        calendar.add(Calendar.MONTH, -1);
        FoodClass foodC = new FoodClass("Minerals", UUID.randomUUID().toString(),24, calendar.getTimeInMillis());

        foodClassList.add(foodA);
        foodClassList.add(foodB);
        foodClassList.add(foodC);

        Log.e("DailyFoodFrag", "loadClassList: count ==>"+foodClassList.size() );
        return foodClassList;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnListFragmentInteractionListener) {
            mListener = (OnListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnListFragmentInteractionListener {
        void onListFragmentInteraction(FoodClass item);
    }
}
