package ng.healthfacts.myperfectweight.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import ng.healthfacts.myperfectweight.R;
import ng.healthfacts.myperfectweight.adapter.SuggestedFoodRecyclerViewAdapter;
import ng.healthfacts.myperfectweight.model.SuggestedFood;
import ng.healthfacts.myperfectweight.model.Tables;

public class BackendListActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    DatabaseReference foodReference;
    String selectedNode = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_backend_list);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(view ->
                startActivity( new Intent(BackendListActivity.this,
                    BackendFormActivity.class)));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        recyclerView = (RecyclerView) findViewById(R.id.backend_list);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        foodReference = FirebaseDatabase.getInstance()
                .getReference(Tables.FOOD_SUGGESTIONS);

        loadDataForNode(Tables.LOSS_SLOW);

    }

    private void loadDataForNode(String node) {
        this.selectedNode = node;
        foodReference.child(node).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                List<SuggestedFood> suggestedFoodList = new ArrayList<>();

                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    SuggestedFood thisFood = snapshot.getValue(SuggestedFood.class);
                    suggestedFoodList.add(thisFood);
                }

                recyclerView.setAdapter(
                        new SuggestedFoodRecyclerViewAdapter
                        (suggestedFoodList, (item, which) -> { onListItemInteraction(item, which);}));

            }
            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void onListItemInteraction(SuggestedFood item, int type) {
        switch (type){
            case SuggestedFoodRecyclerViewAdapter.CLICK:
            case SuggestedFoodRecyclerViewAdapter.LONG_CLICK:
                AlertDialog.Builder builder = new AlertDialog.Builder(this)
                        .setTitle("Food Action")
                        .setMessage("What do you want to do? \n" +
                                "You can edit or delete the selected item.")
                        .setPositiveButton("Delete", ((dialog, which) -> {
                            doDelete(item);
                            dialog.dismiss();
                        }))
                        .setNegativeButton("Edit", ((dialog, which) -> {

                            EditSuggestedFoodFragment fragment = new EditSuggestedFoodFragment();
                            Bundle bundle = new Bundle();
                            bundle.putSerializable("FOOD", item);
                            bundle.putString("WEIGHT", selectedNode);
                            fragment.setArguments(bundle);
                            fragment.show(getSupportFragmentManager(), "EDIT_DIALOG");
                            dialog.dismiss();
                        }))
                        .setNeutralButton("Cancel", (dialog, which) -> dialog.dismiss());
                builder.create().show();
                break;
        }
    }

    public interface OnListFragmentInteractionListener {
        void onListFragmentInteraction(SuggestedFood item, int which);
    }

    private void doDelete(SuggestedFood item){

        AlertDialog.Builder builder = new AlertDialog.Builder(this)
                .setTitle("Food Action")
                .setMessage("Are you sure you want to delete this? \n" +
                        "Continue if you know what you are doing.")
                .setPositiveButton("Yes, Please", ((dialog, which) -> {
                    foodReference.child(this.selectedNode).child(item.getId())
                            .removeValue((databaseError, databaseReference) -> {
                                if( databaseError == null ){
                                    showSnackBar("Food suggestion deleted.");
                                } else {
                                    showSnackBar("Cannot delete this food suggestion at this time.");
                                }
                            });
                    dialog.dismiss();
                }))
                .setNegativeButton("No, thanks", ((dialog, which) -> dialog.dismiss()));
        builder.create().show();

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_backend_switch, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                this.finish();
            case R.id.menu_loss_fast:
                loadDataForNode(Tables.LOSS_FAST);
                break;
            case R.id.menu_loss_slow:
                loadDataForNode(Tables.LOSS_SLOW);
                break;
            case R.id.menu_gain:
                loadDataForNode(Tables.GAIN);
                break;
        }
        return true;
    }

    private void showSnackBar(String s) {
        Snackbar.make(recyclerView, s,
                Snackbar.LENGTH_SHORT).show();
    }
}
