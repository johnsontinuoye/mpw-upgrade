package ng.healthfacts.myperfectweight;

import android.content.ClipData;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.content.ClipboardManager;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static android.content.Context.CLIPBOARD_SERVICE;

public class CaloriesActivity
  extends AppCompatActivity
{
  private Double activity_level;
  Double age;
  EditText age1;
  String agenow;
  Double height;
  EditText height1;
  String heightnow;
  private Toolbar mToolbar;
  private String selectedItemText = null;
  TextView selectedTextView;
  View selectedView;
  Spinner spinner;
  boolean valid;
  Double weight;
  EditText weight1;
  TextView result;
  String weightnow;
  Button copyResult;
  SharedPreferences sharedPreferences;
  private BottomNavigationView navigation;
  private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
          = item -> {
    switch (item.getItemId()) {
      case R.id.navigation_dashboard:
//                        switchFragment( new DashboardFragment() );
        startActivity(new Intent(CaloriesActivity.this, MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION));
        return true;
      case R.id.navigation_shopping_list:
        //switchFragment( new DailyFoodFragment() );
        startActivity(new Intent(CaloriesActivity.this, ShoppingList.class).addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION));
        return true;
      case R.id.navigation_calorie:
        //startActivity(new Intent(CaloriesActivity.this, CaloriesActivity.class));
        return true;
      case R.id.navigation_bmi:
        startActivity(new Intent(CaloriesActivity.this, BMIActivity.class).addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION));
        return true;
    }
    return false;
  };


  public void calculate()
  {
    if (!validate()) {
      return;
    }
    this.agenow = this.age1.getText().toString();
    this.age = Double.valueOf(Double.parseDouble(this.agenow));
    this.heightnow = this.height1.getText().toString();
    this.height = Double.valueOf(Double.parseDouble(this.heightnow));
    this.weightnow = this.weight1.getText().toString();
    this.weight = Double.valueOf(Double.parseDouble(this.weightnow));
    String str = String.format("%.2f", new Object[] { Double.valueOf((447.593D + 9.247D * this.weight.doubleValue() + 3.098D * this.height.doubleValue() - 4.33D * this.age.doubleValue()) * this.activity_level.doubleValue()) });
//    String str = String.format("%.2f", new Object[] { Double.valueOf((447.593D + 9.247D * this.weight.doubleValue() + 3.098D * this.height.doubleValue() - 4.33D * this.age.doubleValue())) });
    new MaterialDialog.Builder(this).title("Your Calorie result").content("Calorie: " + str).positiveText("Ok").show();
    SharedPreferences.Editor localEditor = sharedPreferences.edit();
    localEditor.putString("age", agenow);
    localEditor.putString("height",heightnow);
    localEditor.putString("weight",weightnow);
    localEditor.putString("calories",str);
    localEditor.commit();
    result.setText(str);
  }
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setContentView(R.layout.activity_calorie_calculator);
    sharedPreferences = getSharedPreferences("mypref", 0);
    this.mToolbar = ((Toolbar)findViewById(R.id.toolbar));
    setSupportActionBar(this.mToolbar);
    getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    this.spinner = ((Spinner)findViewById(R.id.spinner));
    Button calorieBtn = (Button)findViewById(R.id.caloriebtn);
    Button localButton = (Button)findViewById(R.id.clearbtn);
    this.age1 = ((EditText)findViewById(R.id.et_age));
    this.height1 = ((EditText)findViewById(R.id.et_height));
    this.weight1 = ((EditText)findViewById(R.id.etWeight));
    this.result = ((TextView)findViewById(R.id.calorie_result));
    if(!sharedPreferences.getString("age","").equalsIgnoreCase("")){
      age1.setText(sharedPreferences.getString("age",""));
    }
    if(!sharedPreferences.getString("height","").equalsIgnoreCase("")){
      height1.setText(sharedPreferences.getString("height",""));
    }
    if(!sharedPreferences.getString("weight","").equalsIgnoreCase("")){
      weight1.setText(sharedPreferences.getString("weight",""));
    }
    if(!sharedPreferences.getString("calories","").equalsIgnoreCase("")){
      result.setText(sharedPreferences.getString("calories",""));
    }
    copyResult = (Button)findViewById(R.id.btn_copy);
    navigation = (BottomNavigationView) findViewById(R.id.navigation);
    navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
    navigation.setSelectedItemId(R.id.navigation_calorie);
    ImageView imageAnim =  (ImageView) findViewById(R.id.imageView3);
    new CountDownTimer(1000, 500) {

      public void onTick(long millisUntilFinished) {
      }

      public void onFinish() {
          Animation in  = AnimationUtils.loadAnimation(CaloriesActivity.this, R.anim.anim_calories_head);
          imageAnim.setAnimation(in);
          imageAnim.setImageResource(R.drawable.ic_calories_head);
      }
    }.start();
    ArrayAdapter local1 = new ArrayAdapter(this, R.layout.spinner_item, new ArrayList(Arrays.asList(new String[] { "Choose your Activity Level", "Sedentary (Little to none)", "Light (1-3 days)", "Moderate (3-5 days)", "Heavy (6-7 days)", "Very heavy (twice daily)" })))
    {
      public View getDropDownView(int paramAnonymousInt, View paramAnonymousView, ViewGroup paramAnonymousViewGroup)
      {
        paramAnonymousView = super.getDropDownView(paramAnonymousInt, paramAnonymousView, paramAnonymousViewGroup);
        TextView view = (TextView)paramAnonymousView;
        if (paramAnonymousInt == 0)
        {
          return view;
        }
        view.setTextColor(R.color.black);
        return view;
      }

      public boolean isEnabled(int paramAnonymousInt)
      {
        return paramAnonymousInt != 0;
      }
    };
    local1.setDropDownViewResource(R.layout.spinner_item);
    this.spinner.setAdapter(local1);
    this.spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
    {
      public void onItemSelected(AdapterView<?> paramAnonymousAdapterView, View paramAnonymousView, int paramAnonymousInt, long paramAnonymousLong)
      {
        selectedItemText = (String)paramAnonymousAdapterView.getItemAtPosition(paramAnonymousInt);
        if (paramAnonymousInt > 0)
        {
          if (CaloriesActivity.this.selectedItemText.contains("Sedentary"))
          {
            activity_level =Double.valueOf(1.2D);
          }
          else if(CaloriesActivity.this.selectedItemText.contains("Light"))
          {
            activity_level = Double.valueOf(1.375D);
          }
          else if (CaloriesActivity.this.selectedItemText.contains("Moderate"))
          {
            activity_level = Double.valueOf(1.55D);
          }
          else if (CaloriesActivity.this.selectedItemText.contains("Heavy"))
          {
            activity_level = Double.valueOf(1.725D);
          }
          else if (CaloriesActivity.this.selectedItemText.contains("twice"))
          {
            activity_level = Double.valueOf(1.9D);
          }
        }
      }

      public void onNothingSelected(AdapterView<?> paramAnonymousAdapterView) {}
    });
    copyResult.setOnClickListener(new View.OnClickListener(){
      public void onClick(View v){
        ClipboardManager clipboard = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText("label", result.getText());
        clipboard.setPrimaryClip(clip);
        Toast.makeText(CaloriesActivity.this, "Copied to clipboard", Toast.LENGTH_SHORT).show();
      }
    });
    localButton.setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        CaloriesActivity.this.age1.setText("");
        CaloriesActivity.this.height1.setText("");
        CaloriesActivity.this.weight1.setText("");
      }
    });
    calorieBtn.setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        CaloriesActivity.this.calculate();
      }
    });
  }
  
  public boolean validate()
  {
    this.valid = true;
    this.agenow = this.age1.getText().toString();
    this.heightnow = this.height1.getText().toString();
    this.weightnow = this.weight1.getText().toString();
    this.selectedView = this.spinner.getSelectedView();
    if (this.selectedItemText.contains("Choose"))
    {
      this.selectedTextView = ((TextView)this.selectedView);
      this.selectedTextView.setError("select an option");
      this.valid = false;
    }
    if (this.agenow.isEmpty()) {
        this.age1.setError("enter your current age");
        this.valid = false;
    }
     else if (this.weightnow.isEmpty()) {
        this.weight1.setError("enter your current weight");
        this.valid = false;
      }
      else if (this.heightnow.isEmpty()) {
        this.height1.setError("enter your current height");
        this.valid = false;

    }else {
      this.age1.setError(null);
      this.weight1.setError(null);
      this.height1.setError(null);
    }
      return this.valid;
  }

  @Override
  protected void onResume() {
    super.onResume();
    navigation.setSelectedItemId(R.id.navigation_calorie);
  }
}
