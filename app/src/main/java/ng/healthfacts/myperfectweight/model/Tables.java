package ng.healthfacts.myperfectweight.model;

/**
 * Created by Afolayan Oluwaseyi on 1/22/18.
 */

public class Tables {

    public static final String USERS = "users";
    public static final String FOOD_SUGGESTIONS = "food_suggestions";
    public static final String LOSS_FAST = "loss_fast";
    public static final String LOSS_SLOW = "loss_slow";
    public static final String GAIN = "gain";
}
