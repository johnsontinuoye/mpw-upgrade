package ng.healthfacts.myperfectweight.model;

import java.io.Serializable;

/**
 * Created by Afolayan Oluwaseyi on 1/17/18.
 */

public class FoodClass implements Serializable {

    private String foodClassName;
    private String id;
    private int quantity;
    private long timeTaken;

    public FoodClass() {
    }

    public FoodClass(String foodClassName, String id, int quantity, long timeTaken) {
        this.foodClassName = foodClassName;
        this.id = id;
        this.quantity = quantity;
        this.timeTaken = timeTaken;
    }

    public String getFoodClassName() {
        return foodClassName;
    }

    public void setFoodClassName(String foodClassName) {
        this.foodClassName = foodClassName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public long getTimeTaken() {
        return timeTaken;
    }

    public void setTimeTaken(long timeTaken) {
        this.timeTaken = timeTaken;
    }
}
