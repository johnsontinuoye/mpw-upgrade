package ng.healthfacts.myperfectweight.model;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.io.Serializable;

/**
 * Created by Afolayan Oluwaseyi on 1/22/18.
 */

@Entity
public class SuggestedFood implements Serializable {


    @PrimaryKey @NonNull
    private String id;
    private String breakfast, lunch, dinner, snack, day;
    @Nullable
    private String snack2, fruit;

    public SuggestedFood() {
    }

    @NonNull
    public String getId() {
        return id;
    }

    public void setId(@NonNull String id) {
        this.id = id;
    }

    public String getBreakfast() {
        return breakfast;
    }

    public void setBreakfast(String breakfast) {
        this.breakfast = breakfast;
    }

    public String getLunch() {
        return lunch;
    }

    public void setLunch(String lunch) {
        this.lunch = lunch;
    }

    public String getDinner() {
        return dinner;
    }

    public void setDinner(String dinner) {
        this.dinner = dinner;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getSnack() {
        return snack;
    }

    public void setSnack(String snack) {
        this.snack = snack;
    }

    @Nullable
    public String getSnack2() {
        return snack2;
    }

    public void setSnack2(@Nullable String snack2) {
        this.snack2 = snack2;
    }

    @Nullable
    public String getFruit() {
        return fruit;
    }

    public void setFruit(@Nullable String fruit) {
        this.fruit = fruit;
    }

    @Override
    public String toString() {
        return "Breakfast: "+breakfast
            + "\nLunch: "+lunch
            + "\nDinner: "+dinner
            + "\nSnack: "+snack
            + "\nSnack2: "+snack2
            + "\nDay: "+day
            + "\nFruit: "+fruit
            + "\nID: "+id;
    }
}
