package ng.healthfacts.myperfectweight.model;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by Afolayan Oluwaseyi on 1/16/18.
 */

public class User implements Serializable{

    private String firstName, lastName;
    private double height, weight;
    private int age;
    private String weightUnit, heightUnit;
    private String activityLevel;
    private String gender;
    private String id, email, password, userName;
    private Login login;
    private String plan;
    private Date datePlanStarted;

    public User() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String username) {
        this.userName = username;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getWeightUnit() {
        return weightUnit;
    }

    public void setWeightUnit(String weightUnit) {
        this.weightUnit = weightUnit;
    }

    public String getHeightUnit() {
        return heightUnit;
    }

    public void setHeightUnit(String heightUnit) {
        this.heightUnit = heightUnit;
    }

    public String getActivityLevel() {
        return activityLevel;
    }

    public void setActivityLevel(String activityLevel) {
        this.activityLevel = activityLevel;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getPlan() {
        return plan;
    }

    public void setPlan(String plan) {
        this.plan = plan;
    }

    public Login getLogin() {
        return login;
    }

    public void setLogin(Login login) {
        this.login = login;
    }

    public Date getDatePlanStarted() {
        return datePlanStarted;
    }

    public void setDatePlanStarted(Date datePlanStarted) {
        this.datePlanStarted = datePlanStarted;
    }

    @Override
    public String toString() {
        return "name: "+lastName +" "+firstName+"\n"
                +"email: "+email+"\n"
                +"username: "+userName+"\n"
                +"password: "+password+"\n"
                +login
                +"gender: "+gender+"\n"
                +"date plan started: "+datePlanStarted+"\n"
                +"plan: "+ plan +"\n"
                +"activity level: "+activityLevel+"\n"
                +"height: "+height+"\n"
                +"height unit: "+heightUnit+"\n"
                +"weight: "+weight+"\n"
                +"weight unit: "+weightUnit;
    }

    public static class Login implements Serializable{
        String userLogin, emailLogin;

        public String getUserLogin() {
            return userLogin;
        }

        public void setUserLogin(String userLogin) {
            this.userLogin = userLogin;
        }

        public String getEmailLogin() {
            return emailLogin;
        }

        public void setEmailLogin(String emailLogin) {
            this.emailLogin = emailLogin;
        }

        @Override
        public String toString() {
            return "Login:\n"
                    +"userLogin: "+userLogin+"\n"
                    +"emailLogin: "+emailLogin+"\n";
        }
    }
}
