package ng.healthfacts.myperfectweight.gain;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.Toolbar;

import ng.healthfacts.myperfectweight.R;

public class Day7
  extends AppCompatActivity
{
  private GridLayoutManager lLayout;
  private Toolbar mToolbar;
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setContentView(R.layout.activity_day7);
    this.mToolbar = ((Toolbar)findViewById(R.id.toolbar));
    setSupportActionBar(this.mToolbar);
    getSupportActionBar().setDisplayHomeAsUpEnabled(true);
  }
}


/* gain\Day7.class
 */