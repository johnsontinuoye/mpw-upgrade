package ng.healthfacts.myperfectweight.db;

import ng.healthfacts.myperfectweight.model.SuggestedFood;

/**
 * Created by Afolayan Oluwaseyi on 1/25/18.
 */

public interface SingleSuggestionListener {
    void onFetchSingleSuggestion(SuggestedFood suggestedFood);
}
