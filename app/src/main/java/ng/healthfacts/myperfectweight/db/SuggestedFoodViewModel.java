package ng.healthfacts.myperfectweight.db;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;
import android.support.annotation.NonNull;

import java.util.List;

import ng.healthfacts.myperfectweight.app.HfWeightDb;
import ng.healthfacts.myperfectweight.model.SuggestedFood;

/**
 * Created by Afolayan Oluwaseyi on 1/25/18.
 */

public class SuggestedFoodViewModel extends AndroidViewModel {

    private HfWeightDb hfWeightDb;
    private LiveData<List<SuggestedFood>> allSuggestions;

    public SuggestedFoodViewModel(@NonNull Application application) {
        super(application);
        hfWeightDb = HfWeightDb.getDatabase(application);
        allSuggestions = hfWeightDb.suggestionsDao().getAllSuggestions();

    }

    public LiveData<List<SuggestedFood>> getAllSuggestions() {
        return allSuggestions;
    }


    public void insertAllSuggestedFoods(List<SuggestedFood> suggestedFoods) {
        new InsertAllSuggestedFoodAsyncTask(suggestedFoods, hfWeightDb).execute();
    }

    public void deleteTable(DeleteSuggestedFoodListener listener) {
        new DeleteAllSuggestedFoodAsyncTask(hfWeightDb, listener).execute();
    }

    public void insertSingleSuggestedFood(SuggestedFood suggestedFood) {
        new InsertASuggestionAsyncTask(suggestedFood, hfWeightDb).execute();
    }

    public void getSuggestionsById(String productId, SingleSuggestionListener listener) {
        new GetSuggestionByIdAsyncTask(hfWeightDb, listener).execute(productId);
    }

    static class InsertAllSuggestedFoodAsyncTask extends AsyncTask<Void, Void, Void>{

        private List<SuggestedFood> suggestedFoods;
        private HfWeightDb db;

        InsertAllSuggestedFoodAsyncTask(List<SuggestedFood> suggestedFoods, HfWeightDb hfWeightDb) {
            this.db = hfWeightDb;
            this.suggestedFoods = suggestedFoods;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            db.suggestionsDao().insertAllSuggestedFoods(suggestedFoods);
            return null;
        }
    }

    static class InsertASuggestionAsyncTask extends AsyncTask<Void, Void, Void> {

        private SuggestedFood suggestedFood;
        private HfWeightDb db;

        InsertASuggestionAsyncTask(SuggestedFood suggestedFood, HfWeightDb hDb) {
            this.suggestedFood = suggestedFood;
            this.db = hDb;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            db.suggestionsDao().insertSuggestedFood(suggestedFood);
            return null;
        }
    }

    static class GetSuggestionByIdAsyncTask extends AsyncTask<String, Void, SuggestedFood> {

        private HfWeightDb db;
        SingleSuggestionListener listener;

        GetSuggestionByIdAsyncTask(HfWeightDb sDb, SingleSuggestionListener listener) {
            this.db = sDb;
            this.listener = listener;
        }

        @Override
        protected SuggestedFood doInBackground(String... strings) {
            return db.suggestionsDao().loadSuggestionsById(strings[0]);
        }

        @Override
        protected void onPostExecute(SuggestedFood suggestedFood) {
            super.onPostExecute(suggestedFood);
            listener.onFetchSingleSuggestion(suggestedFood);
        }
    }
    static class DeleteAllSuggestedFoodAsyncTask extends AsyncTask<Void, Void, Void> {

        private HfWeightDb db;
        DeleteSuggestedFoodListener listener;

        DeleteAllSuggestedFoodAsyncTask(HfWeightDb sDb, DeleteSuggestedFoodListener listener) {
            this.db = sDb;
            this.listener = listener;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            db.suggestionsDao().deleteTable();
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            listener.onDeleteSuggestedFoodTable(true);
        }
    }
}
