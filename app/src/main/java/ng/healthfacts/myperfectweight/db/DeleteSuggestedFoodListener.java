package ng.healthfacts.myperfectweight.db;

/**
 * Created by Afolayan Oluwaseyi on 1/25/18.
 */

public interface DeleteSuggestedFoodListener {
    void onDeleteSuggestedFood(boolean deleted);
    void onDeleteSuggestedFoodTable(boolean deleted);
}
