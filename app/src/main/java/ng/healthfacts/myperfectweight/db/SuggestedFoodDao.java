package ng.healthfacts.myperfectweight.db;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.TypeConverters;

import java.util.List;

import ng.healthfacts.myperfectweight.model.SuggestedFood;

/**
 * Created by Afolayan Oluwaseyi on 1/25/18.
 */

@Dao
@TypeConverters(DateConverter.class)
public interface SuggestedFoodDao {

    @Query("select * from SuggestedFood")
    LiveData<List<SuggestedFood>> getAllSuggestions();

    @Query("select * from SuggestedFood where id = :id")
    SuggestedFood loadSuggestionsById(String id);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAllSuggestedFoods(List<SuggestedFood> suggestedFoods);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertSuggestedFood(SuggestedFood... suggestedFoods);

    @Delete
    void deleteSuggestedFood(SuggestedFood suggestedFood);

    @Query("DELETE FROM SuggestedFood")
    void deleteTable();
}
