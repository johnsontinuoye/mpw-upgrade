package ng.healthfacts.myperfectweight;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import java.util.ArrayList;
import java.util.List;

public class WeightLossFastPlan
  extends AppCompatActivity
{
  public static final String Selection = "weightKey";
  public static final String mypreference = "mypref";
  private GridLayoutManager lLayout;
  private Toolbar mToolbar;
  SharedPreferences sharedpreferences;
  
  private List<ItemObject> getAllItemList()
  {
    ArrayList localArrayList = new ArrayList();
    localArrayList.add(new ItemObject("Day 1"));
    localArrayList.add(new ItemObject("Day 2"));
    localArrayList.add(new ItemObject("Day 3"));
    localArrayList.add(new ItemObject("Day 4"));
    localArrayList.add(new ItemObject("Day 5"));
    localArrayList.add(new ItemObject("Day 6"));
    localArrayList.add(new ItemObject("Day 7"));
    localArrayList.add(new ItemObject("Day 8"));
    localArrayList.add(new ItemObject("Day 9"));
    localArrayList.add(new ItemObject("Day 10"));
    localArrayList.add(new ItemObject("Day 11"));
    localArrayList.add(new ItemObject("Day 12"));
    localArrayList.add(new ItemObject("Day 13"));
    localArrayList.add(new ItemObject("Day 14"));
    localArrayList.add(new ItemObject("Day 15"));
    localArrayList.add(new ItemObject("Day 16"));
    localArrayList.add(new ItemObject("Day 17"));
    localArrayList.add(new ItemObject("Day 18"));
    localArrayList.add(new ItemObject("Day 19"));
    localArrayList.add(new ItemObject("Day 20"));
    localArrayList.add(new ItemObject("Day 21"));
    localArrayList.add(new ItemObject("Day 22"));
    localArrayList.add(new ItemObject("Day 23"));
    localArrayList.add(new ItemObject("Day 24"));
    localArrayList.add(new ItemObject("Day 25"));
    localArrayList.add(new ItemObject("Day 26"));
    localArrayList.add(new ItemObject("Day 27"));
    localArrayList.add(new ItemObject("Day 28"));
    localArrayList.add(new ItemObject("Day 29"));
    localArrayList.add(new ItemObject("Day 30"));
    return localArrayList;
  }
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setContentView(R.layout.activity_weight_loss_fast_plan);
    this.mToolbar = ((Toolbar)findViewById(R.id.toolbar));
    setSupportActionBar(this.mToolbar);
    getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    List itemList = getAllItemList();
    this.lLayout = new GridLayoutManager(this, 3);
    RecyclerView localRecyclerView = (RecyclerView)findViewById(R.id.recycler_view);
    localRecyclerView.setHasFixedSize(true);
    localRecyclerView.setLayoutManager(this.lLayout);
    localRecyclerView.setAdapter(new RecyclerViewAdapterF(this, itemList));
  }
}


/* WeightLossFastPlan.class
 */