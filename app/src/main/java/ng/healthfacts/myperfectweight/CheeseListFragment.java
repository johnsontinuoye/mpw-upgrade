package ng.healthfacts.myperfectweight;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources.Theme;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class CheeseListFragment
  extends Fragment
{
  private List<String> getRandomSublist(String[] paramArrayOfString, int paramInt)
  {
    ArrayList localArrayList = new ArrayList(paramInt);
    Random localRandom = new Random();
    while (localArrayList.size() < paramInt) {
      localArrayList.add(paramArrayOfString[localRandom.nextInt(paramArrayOfString.length)]);
    }
    return localArrayList;
  }
  
  private void setupRecyclerView(RecyclerView paramRecyclerView)
  {
    paramRecyclerView.setLayoutManager(new LinearLayoutManager(paramRecyclerView.getContext()));
    paramRecyclerView.setAdapter(new SimpleStringRecyclerViewAdapter(getActivity(), getRandomSublist(Cheeses.sCheeseStrings, 30)));
  }
  
  @Nullable
  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    RecyclerView list = (RecyclerView)paramLayoutInflater.inflate(R.layout.fragment_cheese_list, paramViewGroup, false);
    setupRecyclerView(list);
    return list;
  }
  
  public class SimpleStringRecyclerViewAdapter
    extends RecyclerView.Adapter<SimpleStringRecyclerViewAdapter.ViewHolder>
  {
    private int mBackground;
    private final TypedValue mTypedValue = new TypedValue();
    private List<String> mValues;

    public SimpleStringRecyclerViewAdapter(Context paramContext, List<String> paramList)
    {
      paramContext.getTheme().resolveAttribute(2130968948, this.mTypedValue, true);
      this.mBackground = this.mTypedValue.resourceId;
      this.mValues = paramList;
    }
    
    public int getItemCount()
    {
      return this.mValues.size();
    }
    
    public void onBindViewHolder(@NonNull ViewHolder paramViewHolder, int paramInt)
    {
      paramViewHolder.mBoundString = ((String)this.mValues.get(paramInt));
      paramViewHolder.mTextView.setText((CharSequence)this.mValues.get(paramInt));
      paramViewHolder.mView.setOnClickListener(new View.OnClickListener()
      {
        public void onClick(View paramAnonymousView)
        {
          Context context = paramAnonymousView.getContext();
          Intent localIntent = new Intent(context, CheeseDetailActivity.class);
          localIntent.putExtra("cheese_name", paramViewHolder.mBoundString);
          startActivity(localIntent);
        }
      });
      //Glide.with(paramViewHolder.mImageView.getContext()).load(Integer.valueOf(Cheeses.getRandomCheeseDrawable())).fitCenter().into(paramViewHolder.mImageView);
      Glide.with(paramViewHolder.mImageView.getContext()).load(Integer.valueOf(Cheeses.getRandomCheeseDrawable())).into(paramViewHolder.mImageView);
    }
    
    public ViewHolder onCreateViewHolder(ViewGroup paramViewGroup, int paramInt)
    {
      View layoutInflater = LayoutInflater.from(paramViewGroup.getContext()).inflate(R.layout.list_item, paramViewGroup, false);
      paramViewGroup.setBackgroundResource(this.mBackground);
      return new ViewHolder(paramViewGroup);
    }
    
    public class ViewHolder
      extends RecyclerView.ViewHolder
    {
      public String mBoundString;
      public final ImageView mImageView;
      public final TextView mTextView;
      public final View mView;
      
      public ViewHolder(View paramView)
      {
        super(paramView);
        this.mView = paramView;
        this.mImageView = ((ImageView)paramView.findViewById(R.id.avatar));
        this.mTextView = ((TextView)paramView.findViewById(R.id.text1));
      }
      
      public String toString()
      {
        return super.toString() + " '" + this.mTextView.getText();
      }
    }
  }
}
