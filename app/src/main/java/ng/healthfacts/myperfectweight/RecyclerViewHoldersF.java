package ng.healthfacts.myperfectweight;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;
import ng.healthfacts.myperfectweight.fast.FDay1;
import ng.healthfacts.myperfectweight.fast.FDay10;
import ng.healthfacts.myperfectweight.fast.FDay11;
import ng.healthfacts.myperfectweight.fast.FDay12;
import ng.healthfacts.myperfectweight.fast.FDay13;
import ng.healthfacts.myperfectweight.fast.FDay14;
import ng.healthfacts.myperfectweight.fast.FDay15;
import ng.healthfacts.myperfectweight.fast.FDay16;
import ng.healthfacts.myperfectweight.fast.FDay17;
import ng.healthfacts.myperfectweight.fast.FDay18;
import ng.healthfacts.myperfectweight.fast.FDay19;
import ng.healthfacts.myperfectweight.fast.FDay2;
import ng.healthfacts.myperfectweight.fast.FDay20;
import ng.healthfacts.myperfectweight.fast.FDay21;
import ng.healthfacts.myperfectweight.fast.FDay22;
import ng.healthfacts.myperfectweight.fast.FDay23;
import ng.healthfacts.myperfectweight.fast.FDay24;
import ng.healthfacts.myperfectweight.fast.FDay25;
import ng.healthfacts.myperfectweight.fast.FDay26;
import ng.healthfacts.myperfectweight.fast.FDay27;
import ng.healthfacts.myperfectweight.fast.FDay28;
import ng.healthfacts.myperfectweight.fast.FDay29;
import ng.healthfacts.myperfectweight.fast.FDay3;
import ng.healthfacts.myperfectweight.fast.FDay30;
import ng.healthfacts.myperfectweight.fast.FDay4;
import ng.healthfacts.myperfectweight.fast.FDay5;
import ng.healthfacts.myperfectweight.fast.FDay6;
import ng.healthfacts.myperfectweight.fast.FDay7;
import ng.healthfacts.myperfectweight.fast.FDay8;
import ng.healthfacts.myperfectweight.fast.FDay9;

public class RecyclerViewHoldersF
  extends RecyclerView.ViewHolder
  implements View.OnClickListener
{
  public static final String Selection = "weightKey";
  public static final String mypreference = "mypref";
  public Context context;
  public TextView daytitle;
  SharedPreferences sharedpreferences;
  
  public RecyclerViewHoldersF(View paramView)
  {
    super(paramView);
    paramView.setOnClickListener(this);
    this.daytitle = ((TextView)paramView.findViewById(R.id.tvTitle));
  }
  
  public void onClick(View paramView)
  {
    switch (getPosition())
    {
    default: 
      this.context = paramView.getContext();
      this.context.startActivity(new Intent(this.context, FDay30.class));
      return;
    case 0: 
      this.context = paramView.getContext();
      this.context.startActivity(new Intent(this.context, FDay1.class));
      return;
    case 1: 
      this.context = paramView.getContext();
      this.context.startActivity(new Intent(this.context, FDay2.class));
      return;
    case 2: 
      this.context = paramView.getContext();
      this.context.startActivity(new Intent(this.context, FDay3.class));
      return;
    case 3: 
      this.context = paramView.getContext();
      this.context.startActivity(new Intent(this.context, FDay4.class));
      return;
    case 4: 
      this.context = paramView.getContext();
      this.context.startActivity(new Intent(this.context, FDay5.class));
      return;
    case 5: 
      this.context = paramView.getContext();
      this.context.startActivity(new Intent(this.context, FDay6.class));
      return;
    case 6: 
      this.context = paramView.getContext();
      this.context.startActivity(new Intent(this.context, FDay7.class));
      return;
    case 7: 
      this.context = paramView.getContext();
      this.context.startActivity(new Intent(this.context, FDay8.class));
      return;
    case 8: 
      this.context = paramView.getContext();
      this.context.startActivity(new Intent(this.context, FDay9.class));
      return;
    case 9: 
      this.context = paramView.getContext();
      this.context.startActivity(new Intent(this.context, FDay10.class));
      return;
    case 10: 
      this.context = paramView.getContext();
      this.context.startActivity(new Intent(this.context, FDay11.class));
      return;
    case 11: 
      this.context = paramView.getContext();
      this.context.startActivity(new Intent(this.context, FDay12.class));
      return;
    case 12: 
      this.context = paramView.getContext();
      this.context.startActivity(new Intent(this.context, FDay13.class));
      return;
    case 13: 
      this.context = paramView.getContext();
      this.context.startActivity(new Intent(this.context, FDay14.class));
      return;
    case 14: 
      this.context = paramView.getContext();
      this.context.startActivity(new Intent(this.context, FDay15.class));
      return;
    case 15: 
      this.context = paramView.getContext();
      this.context.startActivity(new Intent(this.context, FDay16.class));
      return;
    case 16: 
      this.context = paramView.getContext();
      this.context.startActivity(new Intent(this.context, FDay17.class));
      return;
    case 17: 
      this.context = paramView.getContext();
      this.context.startActivity(new Intent(this.context, FDay18.class));
      return;
    case 18: 
      this.context = paramView.getContext();
      this.context.startActivity(new Intent(this.context, FDay19.class));
      return;
    case 19: 
      this.context = paramView.getContext();
      this.context.startActivity(new Intent(this.context, FDay20.class));
      return;
    case 20: 
      this.context = paramView.getContext();
      this.context.startActivity(new Intent(this.context, FDay21.class));
      return;
    case 21: 
      this.context = paramView.getContext();
      this.context.startActivity(new Intent(this.context, FDay22.class));
      return;
    case 22: 
      this.context = paramView.getContext();
      this.context.startActivity(new Intent(this.context, FDay23.class));
      return;
    case 23: 
      this.context = paramView.getContext();
      this.context.startActivity(new Intent(this.context, FDay24.class));
      return;
    case 24: 
      this.context = paramView.getContext();
      this.context.startActivity(new Intent(this.context, FDay25.class));
      return;
    case 25: 
      this.context = paramView.getContext();
      this.context.startActivity(new Intent(this.context, FDay26.class));
      return;
    case 26: 
      this.context = paramView.getContext();
      this.context.startActivity(new Intent(this.context, FDay27.class));
      return;
    case 27: 
      this.context = paramView.getContext();
      this.context.startActivity(new Intent(this.context, FDay28.class));
      return;
      case 28:
        this.context = paramView.getContext();
        this.context.startActivity(new Intent(this.context, FDay29.class));
    }
  }
}


/* RecyclerViewHoldersF.class
 */