package ng.healthfacts.myperfectweight;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.afollestad.materialdialogs.MaterialDialog.Builder;

import static android.content.Context.CLIPBOARD_SERVICE;

public class BMIActivity
  extends AppCompatActivity
{
  private Double activity_level;
  Double age;
  EditText age1;
  String agenow;
  TextView tv_bmi;
  TextView tv_status;
  Double height;
  EditText height1;
  String heightnow;
  Button copyResult;
  private Toolbar mToolbar;
  private String selectedItemText = null;
  TextView selectedTextView;
  View selectedView;
  String status;
  boolean valid;
  Double weight;
  EditText weight1;
  String weightnow;
  SharedPreferences sharedPreferences;
  private BottomNavigationView navigation;
  private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
          = item -> {
    switch (item.getItemId()) {
      case R.id.navigation_dashboard:
//                        switchFragment( new DashboardFragment() );
        startActivity(new Intent(BMIActivity.this, MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION));
        return true;
      case R.id.navigation_shopping_list:
        //switchFragment( new DailyFoodFragment() );
        startActivity(new Intent(BMIActivity.this, ShoppingList.class).addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION));
        return true;
      case R.id.navigation_calorie:
        startActivity(new Intent(BMIActivity.this, CaloriesActivity.class).addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION));
        return true;
      case R.id.navigation_bmi:
        //startActivity(new Intent(BMIActivity.this, BMIActivity.class));
        return true;
    }
    return false;
  };
  
  public void calculate()
  {
    if (!validate())
    {
      Toast.makeText(this, "Calorie ", Toast.LENGTH_SHORT).show();
      return;
    }
    this.agenow = this.age1.getText().toString();
    this.age = Double.valueOf(Double.parseDouble(this.agenow));
    this.heightnow = this.height1.getText().toString();
    this.height = Double.valueOf(Double.parseDouble(this.heightnow));
    this.weightnow = this.weight1.getText().toString();
    this.weight = Double.valueOf(Double.parseDouble(this.weightnow));
    Double localDouble = Double.valueOf(10000.0D * this.weight.doubleValue() / (this.height.doubleValue() * this.height.doubleValue()));
    String str = String.format("%.2f", new Object[] { localDouble });
    if (localDouble.doubleValue() <= 18.5D) {
      this.status = "Underweight";
    }
    if ((localDouble.doubleValue() <= 24.9D) && (localDouble.doubleValue() >= 18.5D)) {
        this.status = "Normal weight";
      } else if ((localDouble.doubleValue() <= 29.9D) && (localDouble.doubleValue() >= 25.0D)) {
        this.status = "Over weight";
      } else if (localDouble.doubleValue() >= 30.0D) {
        this.status = "Obese";
      }
    tv_bmi.setText(str);
    tv_status.setText(this.status);

    SharedPreferences.Editor localEditor = sharedPreferences.edit();
    localEditor.putString("age", agenow);
    localEditor.putString("height",heightnow);
    localEditor.putString("weight",weightnow);
    localEditor.putString("bmi",str);
    localEditor.putString("bmi_status",status);
    localEditor.commit();
      new MaterialDialog.Builder(this).title("Your BMI result").content("BMI: " + str + "\nStatus: " + this.status).positiveText("Ok").show();
  }
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setContentView(R.layout.activity_bmi_calculator);
    this.mToolbar = ((Toolbar)findViewById(R.id.toolbar));
    setSupportActionBar(this.mToolbar);
    getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    Button calorieBtn = (Button)findViewById(R.id.caloriebtn);
    Button localButton = (Button)findViewById(R.id.clearbtn);
    sharedPreferences = getSharedPreferences("mypref", 0);
    this.age1 = ((EditText)findViewById(R.id.et_age));
    this.height1 = ((EditText)findViewById(R.id.et_height));
    this.weight1 = ((EditText)findViewById(R.id.etWeight));
    tv_bmi = (TextView)findViewById(R.id.tv_bmi);
    tv_status = (TextView)findViewById(R.id.tv_status);
    navigation = (BottomNavigationView) findViewById(R.id.navigation);
    navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
    navigation.setSelectedItemId(R.id.navigation_bmi);
    if(!sharedPreferences.getString("age","").equalsIgnoreCase("")){
      age1.setText(sharedPreferences.getString("age",""));
    }
    if(!sharedPreferences.getString("height","").equalsIgnoreCase("")){
      height1.setText(sharedPreferences.getString("height",""));
    }
    if(!sharedPreferences.getString("weight","").equalsIgnoreCase("")){
      weight1.setText(sharedPreferences.getString("weight",""));
    }
    if(!sharedPreferences.getString("bmi","").equalsIgnoreCase("")){
      tv_bmi.setText(sharedPreferences.getString("bmi",""));
    }
    if(!sharedPreferences.getString("bmi_status","").equalsIgnoreCase("")){
      tv_status.setText(sharedPreferences.getString("bmi_status",""));
    }

    ImageView imageAnim =  (ImageView) findViewById(R.id.imageView3);
    new CountDownTimer(1000, 500) {

      public void onTick(long millisUntilFinished) {
      }

      public void onFinish() {
        Animation in  = AnimationUtils.loadAnimation(BMIActivity.this, R.anim.anim_calories_head);
        imageAnim.setAnimation(in);
        imageAnim.setImageResource(R.drawable.ic_fitness_center_head);
      }
    }.start();
    copyResult = (Button)findViewById(R.id.btn_copy);
    copyResult.setOnClickListener(new View.OnClickListener(){
      public void onClick(View v){
        ClipboardManager clipboard = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText("label", tv_bmi.getText());
        clipboard.setPrimaryClip(clip);
        Toast.makeText(BMIActivity.this, "Copied to clipboard", Toast.LENGTH_SHORT).show();
      }
    });
    localButton.setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        BMIActivity.this.age1.setText("");
        BMIActivity.this.height1.setText("");
        BMIActivity.this.weight1.setText("");
      }
    });
    calorieBtn.setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        BMIActivity.this.calculate();
      }
    });
  }
  
  public boolean validate()
  {
    this.valid = true;
    this.agenow = this.age1.getText().toString();
    this.heightnow = this.height1.getText().toString();
    this.weightnow = this.weight1.getText().toString();
    if (this.agenow.isEmpty())
    {
      this.age1.setError("enter your current age");
      this.valid = false;
      if (this.weightnow.isEmpty()) {
        this.weight1.setError("enter your current weight");
        this.valid = false;
      }
      if (!this.heightnow.isEmpty()) {
        this.height1.setError("enter your current height in cms");
        this.valid = false;
      }
    }else {
      this.age1.setError(null);
      this.weight1.setError(null);
      this.height1.setError(null);
    }
    return this.valid;
  }

  @Override
  protected void onResume() {
    super.onResume();
    navigation.setSelectedItemId(R.id.navigation_bmi);
  }
}