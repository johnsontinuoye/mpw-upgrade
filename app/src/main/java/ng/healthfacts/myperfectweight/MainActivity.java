package ng.healthfacts.myperfectweight;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.NavigationView;
import android.support.design.widget.NavigationView.OnNavigationItemSelectedListener;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;
import java.util.ArrayList;
import java.util.List;

public class MainActivity
  extends AppCompatActivity
{
  public static final String Selection = "weightKey";
  public static final String mypreference = "mypref";
  private BottomNavigationView navigation;
  private DrawerLayout mDrawerLayout;
  SharedPreferences sharedpreferences;
  private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
          = item -> {
    switch (item.getItemId()) {
      case R.id.navigation_dashboard:
//                        switchFragment( new DashboardFragment() );
      // startActivity(new Intent(MainActivity.this, PlanSelectionActivity.class));
        return true;
      case R.id.navigation_shopping_list:
        //switchFragment( new DailyFoodFragment() );
        startActivity(new Intent(MainActivity.this, ShoppingList.class).addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION));
        return true;
      case R.id.navigation_calorie:
        startActivity(new Intent(MainActivity.this, CaloriesActivity.class).addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION));
        return true;
      case R.id.navigation_bmi:
        startActivity(new Intent(MainActivity.this, BMIActivity.class).addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION));
        return true;
    }
    return false;
  };
  
  private void setNightMode(int paramInt)
  {
    AppCompatDelegate.setDefaultNightMode(paramInt);
    if (Build.VERSION.SDK_INT >= 17) {
      recreate();
    }
  }
  
  private void setupDrawerContent(NavigationView paramNavigationView)
  {
    paramNavigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener()
    {
      public boolean onNavigationItemSelected(MenuItem paramAnonymousMenuItem)
      {
        paramAnonymousMenuItem.setChecked(true);
       // MainActivity.this.mDrawerLayout.closeDrawers();
        switch (paramAnonymousMenuItem.getItemId())
        {
        default: 
          return true;
        case 2131296487: 
          sharedpreferences = MainActivity.this.getSharedPreferences("mypref", 0);
          String weightKey = sharedpreferences.getString("weightKey", "");
          Toast.makeText(MainActivity.this, "You are on " + weightKey + " Plan", Toast.LENGTH_SHORT).show();
          if (weightKey.contains("Gain"))
          {
            Intent intent = new Intent(MainActivity.this, WeightGainPlan.class);
            MainActivity.this.startActivity(intent);
            return true;
          }
          if (weightKey.contains("Fast"))
          {
            Intent intent = new Intent(MainActivity.this, WeightLossFastPlan.class);
            MainActivity.this.startActivity(intent);
            return true;
          }
          if (weightKey.contains("Slow"))
          {
            Intent intent = new Intent(MainActivity.this, WeightLossSlowPlan.class);
            MainActivity.this.startActivity(intent);
            return true;
          }
          Intent intent = new Intent(MainActivity.this, PlanSelectionActivity.class);
          MainActivity.this.startActivity(intent);
          return true;
        case 2131296488: 
          Intent intent1 = new Intent(MainActivity.this, ShoppingList.class);
          MainActivity.this.startActivity(intent1);
          return true;
        case 2131296486: 
          Intent intent2 = new Intent(MainActivity.this, CaloriesActivity.class);
          MainActivity.this.startActivity(intent2);
          return true;
        }
//        Intent intent = new Intent(MainActivity.this, BMIActivity.class);
//        MainActivity.this.startActivity(intent);
//        return true;
      }
    });
  }
  
  private void setupViewPager(ViewPager paramViewPager)
  {
    Adapter localAdapter = new Adapter(getSupportFragmentManager());
    localAdapter.addFragment(new CheeseListFragment(), "Category 1");
    paramViewPager.setAdapter(localAdapter);
  }
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setContentView(R.layout.activity_main);
    setSupportActionBar((Toolbar)findViewById(R.id.toolbar));
    Object localObject = getSupportActionBar();
    ((ActionBar)localObject).setHomeAsUpIndicator(R.drawable.ic_menu);
    ((ActionBar)localObject).setDisplayHomeAsUpEnabled(true);
    //this.mDrawerLayout = ((DrawerLayout)findViewById(R.id.drawer_layout));
    //localObject = (NavigationView)findViewById(R.id.nav_view);
//    if (localObject != null) {
//      setupDrawerContent((NavigationView)localObject);
//    }

    navigation = (BottomNavigationView) findViewById(R.id.navigation);
    navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
    navigation.setSelectedItemId(R.id.navigation_dashboard);

    if (paramBundle == null)
    {
      FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
      ft.replace(R.id.frame, new DashboardFragment());
      ft.commit();
    }
  }
  
  public boolean onCreateOptionsMenu(Menu paramMenu)
  {
    getMenuInflater().inflate(R.menu.sample_actions, paramMenu);
    return true;
  }
  
  public boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
//    switch (paramMenuItem.getItemId())
//    {
//      default:
//      {return super.onOptionsItemSelected(paramMenuItem);}
//        this.mDrawerLayout.openDrawer(8388611);
//        return true;
//        case
//      setNightMode(-1);
//      return true;
//      setNightMode(1);
//      continue;
//      setNightMode(2);
//      continue;
//      setNightMode(0);
//    }
    return true;
  }
  
  public boolean onPrepareOptionsMenu(Menu paramMenu)
  {
//    switch ()
//    {
//    default:
//      return true;
//    case -1:
//      paramMenu.findItem(2131296478).setChecked(true);
//      return true;
//    case 0:
//      paramMenu.findItem(2131296475).setChecked(true);
//      return true;
//    case 2:
//      paramMenu.findItem(2131296477).setChecked(true);
//      return true;
//    }
//    paramMenu.findItem(2131296476).setChecked(true);
     return true;
  }
  
  static class Adapter
    extends FragmentPagerAdapter
  {
    private final List<String> mFragmentTitles = new ArrayList();
    private final List<Fragment> mFragments = new ArrayList();
    
    public Adapter(FragmentManager paramFragmentManager)
    {
      super(paramFragmentManager);
    }
    
    public void addFragment(Fragment paramFragment, String paramString)
    {
      this.mFragments.add(paramFragment);
      this.mFragmentTitles.add(paramString);
    }
    
    public int getCount()
    {
      return this.mFragments.size();
    }
    
    public Fragment getItem(int paramInt)
    {
      return (Fragment)this.mFragments.get(paramInt);
    }
    
    public CharSequence getPageTitle(int paramInt)
    {
      return (CharSequence)this.mFragmentTitles.get(paramInt);
    }
  }

  @Override
  protected void onResume() {
    super.onResume();
    navigation.setSelectedItemId(R.id.navigation_dashboard);
  }
}


/* MainActivity.class
 */