package ng.healthfacts.myperfectweight;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.TextView;
import java.util.List;

public class RecyclerViewAdapter
  extends RecyclerView.Adapter<RecyclerViewHolders>
{
  public Context context;
  public List<ItemObject> itemList;
  
  public RecyclerViewAdapter(Context paramContext, List<ItemObject> paramList)
  {
    this.itemList = paramList;
    this.context = paramContext;
  }
  
  public int getItemCount()
  {
    return this.itemList.size();
  }
  
  public void onBindViewHolder(RecyclerViewHolders paramRecyclerViewHolders, int paramInt)
  {
    paramRecyclerViewHolders.daytitle.setText(((ItemObject)this.itemList.get(paramInt)).getDay());
  }
  
  public RecyclerViewHolders onCreateViewHolder(ViewGroup paramViewGroup, int paramInt)
  {
    return new RecyclerViewHolders(LayoutInflater.from(paramViewGroup.getContext()).inflate(R.layout.recyclerview_item_row, null));
  }
}
