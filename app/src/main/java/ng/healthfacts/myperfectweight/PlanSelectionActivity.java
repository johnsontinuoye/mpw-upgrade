package ng.healthfacts.myperfectweight;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AlertDialog.Builder;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import devs.mulham.horizontalcalendar.HorizontalCalendar;
import devs.mulham.horizontalcalendar.HorizontalCalendarView;
import devs.mulham.horizontalcalendar.utils.HorizontalCalendarListener;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class PlanSelectionActivity
  extends AppCompatActivity
  implements AdapterView.OnItemSelectedListener
{
  public static final String Selection = "weightKey";
  public static final String TAG = PlanSelectionActivity.class.getSimpleName();
  public static final String mypreference = "mypref";
  private Button btnJoin;
  HorizontalCalendarView horizontalCalendarView;
  SharedPreferences sharedpreferences;
  TextView tvBanner;
  TextView tvContent;
  TextView tvHeader;
  TextView weightGain;
  TextView weightLossFast;
  TextView weightLossSlow;
  Spinner weightSelectionSpinner;
  String[] weightSelectionString;
  private BottomNavigationView navigation;
  private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
          = item -> {
    switch (item.getItemId()) {
      case R.id.navigation_dashboard:
//                        switchFragment( new DashboardFragment() );
        startActivity(new Intent(PlanSelectionActivity.this, MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK));
        return true;
      case R.id.navigation_shopping_list:
        //switchFragment( new DailyFoodFragment() );
        startActivity(new Intent(PlanSelectionActivity.this, ShoppingList.class).addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION));
        return true;
      case R.id.navigation_calorie:
        startActivity(new Intent(PlanSelectionActivity.this, CaloriesActivity.class).addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION));
        return true;
      case R.id.navigation_bmi:
        startActivity(new Intent(PlanSelectionActivity.this, BMIActivity.class).addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION));
        return true;
    }
    return false;
  };

  private void onJoinButtonClicked(String paramString)
  {
    Log.e(TAG, "onClick: selected weight " + paramString);
    SharedPreferences.Editor localEditor = this.sharedpreferences.edit();
    localEditor.putString("weightKey", paramString);
    localEditor.commit();
    new AlertDialog.Builder(this).setTitle("Subscription").setMessage("You have successfully subscribed to the " + paramString + " plan. Welcome on board").setPositiveButton("Dismiss", new DialogInterface.OnClickListener() {
      @Override
      public void onClick(DialogInterface dialogInterface, int i) {

        Intent intent = new Intent(PlanSelectionActivity.this, MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PlanSelectionActivity.this.startActivity(intent);
        Log.e(TAG, "onClick: selected weight ");
        dialogInterface.cancel();
      }
    }).create().show();
  }
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setContentView(R.layout.activity_select_plan);
    this.sharedpreferences = getSharedPreferences("mypref", 0);
    //this.horizontalCalendarView = ((HorizontalCalendarView)findViewById(R.id.calendarView));
    //Calendar calendar = Calendar.getInstance();
//    calendar.add(Calendar.MONTH, -1);
//    Calendar localCalendar = Calendar.getInstance();
//    calendar.add(Calendar.MONTH, 1);
//    setSupportActionBar((Toolbar)findViewById(R.id.toolbar));

    navigation = (BottomNavigationView) findViewById(R.id.navigation);
    navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
    this.btnJoin = ((Button)findViewById(R.id.btn_join));
    this.weightSelectionString = getResources().getStringArray(R.array.weight_selection_values);
//   final SimpleDateFormat localSimpleDateFormat = new SimpleDateFormat("MMMM dd, yyyy", Locale.getDefault());
//    new HorizontalCalendar.Builder(this, R.id.calendarView).range(calendar, localCalendar).datesNumberOnScreen(7).build().setCalendarListener(new HorizontalCalendarListener()
//    {
//      public void onCal endarScroll(HorizontalCalendarView paramAnonymousHorizontalCalendarView, int paramAnonymousInt1, int paramAnonymousInt2)
//      {
//        super.onCalendarScroll(paramAnonymousHorizontalCalendarView, paramAnonymousInt1, paramAnonymousInt2);
//        Log.e(PlanSelectionActivity.TAG, "onCalendarScroll: dx:dy=" + paramAnonymousInt1 + " : " + paramAnonymousInt2);
//      }
//
//      public boolean onDateLongClicked(Calendar paramAnonymousCalendar, int paramAnonymousInt)
//      {
//        Log.e(PlanSelectionActivity.TAG, "onCalendarScroll: date:position =" + paramAnonymousCalendar + " : " + paramAnonymousInt);
//        return super.onDateLongClicked(paramAnonymousCalendar, paramAnonymousInt);
//      }
//
//      public void onDateSelected(Calendar paramAnonymousCalendar, int paramAnonymousInt)
//      {
//        Log.e(PlanSelectionActivity.TAG, "onDateSelected: date: " + localSimpleDateFormat.format(paramAnonymousCalendar));
//      }
//    });
//    this.weightSelectionSpinner = ((Spinner)findViewById(R.id.spinner_weight_plan));
//    this.weightSelectionSpinner.setOnItemSelectedListener(this);
//    this.tvHeader = ((TextView)findViewById(R.id.tv_header_text));
//    this.tvBanner = ((TextView)findViewById(R.id.tv_header_banner));
//    this.tvContent = ((TextView)findViewById(R.id.tv_content));
    weightGain = (TextView)findViewById(R.id.tv_weight_gain);
    weightGain.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        Log.e(TAG, "onClick: selected weight " + weightSelectionString[0]);
        SharedPreferences.Editor localEditor = sharedpreferences.edit();
        localEditor.putString("weightKey", weightSelectionString[0]);
        localEditor.putLong("start_day",Calendar.getInstance().getTimeInMillis());
        localEditor.commit();
        new AlertDialog.Builder(PlanSelectionActivity.this).setTitle("Subscription").setMessage("You have successfully subscribed to the " + weightSelectionString[0] + " plan. Welcome on board").setPositiveButton("Dismiss", new DialogInterface.OnClickListener() {
          @Override
          public void onClick(DialogInterface dialogInterface, int i) {

            Intent intent = new Intent(PlanSelectionActivity.this, MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            Toast.makeText(PlanSelectionActivity.this, "You are now on " + weightSelectionString[0] + " Plan", Toast.LENGTH_SHORT).show();
            PlanSelectionActivity.this.startActivity(intent);
            dialogInterface.cancel();
          }
        }).create().show();
      }
    });

    weightLossFast = (TextView)findViewById(R.id.tv_weight_loss_fast);
    weightLossFast.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        Log.e(TAG, "onClick: selected weight " + weightSelectionString[1]);
        SharedPreferences.Editor localEditor = sharedpreferences.edit();
        localEditor.putString("weightKey", weightSelectionString[1]);
        localEditor.putLong("start_day",Calendar.getInstance().getTimeInMillis());
        localEditor.commit();
        new AlertDialog.Builder(PlanSelectionActivity.this).setTitle("Subscription").setMessage("You have successfully subscribed to the " + weightSelectionString[1] + " plan. Welcome on board").setPositiveButton("Dismiss", new DialogInterface.OnClickListener() {
          @Override
          public void onClick(DialogInterface dialogInterface, int i) {

            Intent intent = new Intent(PlanSelectionActivity.this, MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            Toast.makeText(PlanSelectionActivity.this, "You are now on " + weightSelectionString[1] + " Plan", Toast.LENGTH_SHORT).show();
            PlanSelectionActivity.this.startActivity(intent);
            dialogInterface.cancel();
          }
        }).create().show();
      }
    });

    weightLossSlow = (TextView)findViewById(R.id.tv_weight_loss_slow);
    weightLossSlow.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        Log.e(TAG, "onClick: selected weight " + weightSelectionString[2]);
        SharedPreferences.Editor localEditor = sharedpreferences.edit();
        localEditor.putString("weightKey", weightSelectionString[2]);
        localEditor.putLong("start_day",Calendar.getInstance().getTimeInMillis());
        localEditor.commit();
        new AlertDialog.Builder(PlanSelectionActivity.this).setTitle("Subscription").setMessage("You have successfully subscribed to the " + weightSelectionString[2] + " plan. Welcome on board").setPositiveButton("Dismiss", new DialogInterface.OnClickListener() {
          @Override
          public void onClick(DialogInterface dialogInterface, int i) {
            Intent intent = new Intent(PlanSelectionActivity.this, MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            Toast.makeText(PlanSelectionActivity.this, "You are now on " + weightSelectionString[2] + " Plan", Toast.LENGTH_SHORT).show();
            PlanSelectionActivity.this.startActivity(intent);
            dialogInterface.cancel();
          }
        }).create().show();
      }
    });
  }
  
  public void onItemSelected(AdapterView<?> paramAdapterView, View paramView, int paramInt, long paramLong)
  {
//    String selectionString = this.weightSelectionString[paramInt];
//    this.tvHeader.setText(selectionString);
//    this.tvBanner.setText(selectionString);
//    this.tvContent.setText(selectionString);
    //this.btnJoin.setOnClickListener(new PlanSelectionActivity..Lambda.0(this, paramAdapterView));
//    this.btnJoin.setOnClickListener(new View.OnClickListener() {
//      @Override
//      public void onClick(View view) {
//        onJoinButtonClicked(selectionString);
//      }
//    });
    }
  
  public void onNothingSelected(AdapterView<?> paramAdapterView) {}
  
  public boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    switch (paramMenuItem.getItemId())
    {
    }
    return true;
  }
}